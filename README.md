# OpenXes.Net

## Introduction

OpenXes.Net project is a .NET c# implementation of the eXtensible Event Stream (XES) standard, based on the reference Java implemenation ([OpenXes](http://www.xes-standard.org/openxes/start)) provided by their creators.

This library conforms to the version [2.0](http://www.xes-standard.org/_media/xes/xesstandarddefinition-2.0.pdf) of the XES standard and it is distributed as an opensource project under the [GNU Lesser GPL](http://www.xes-standard.org/downloads/LICENSE.txt) license


## Changelog

### Version 1.0.1 (December 2017)
Changed the target framework from netcore2.0. to netstandard2.0 for a better compatibility with .Net projects when importing the generated nuget package
Modified links to a new repo source

### Version 1.0.0 (December 2017)
Complete porting of the Java implementation, except for the support of buffered logs implementation.

## Contributing
Anyone willing to improve this library is wellcome. You can fork the project from https://www.bitbucket.org/sabien/openxes.net.git or ask for access as contributor.

## Notifying bugs
Use our [bug tracker](https://bitbucket.org/sabien/openxes.net/issues) to create a new bug report.