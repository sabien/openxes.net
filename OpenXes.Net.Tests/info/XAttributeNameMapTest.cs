﻿using Xunit;
using Xunit.Abstractions;

namespace OpenXesNet.info
{
    public class XAttributeNameMapTest
    {

        ITestOutputHelper output;

        public XAttributeNameMapTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void CreateInstance()
        {
            XAttributeNameMap map = new XAttributeNameMap("MyMap");
            Assert.NotNull(map);
            Assert.Equal("MyMap", map.MappingName);

            // Should be empty
            Assert.Empty(map);
        }

        public class EmptyMap
        {

            [Fact]
            public void TestToString()
            {
                XAttributeNameMap map = new XAttributeNameMap("MyMap");

                string mapstr = map.ToString();

                Assert.Matches("Attribute name map: MyMap", mapstr);
            }

            [Fact]
            public void TestRegisterMapping()
            {
                XAttributeNameMap map = new XAttributeNameMap("MyMap");
                map.RegisterMapping("key", "value");
                Assert.Single(map);
                Assert.True(map.ContainsKey("key"));
                Assert.True(map.ContainsValue("value"));
            }

            [Fact]
            public void GettingMapping()
            {
                XAttributeNameMap map = new XAttributeNameMap("MyMap");
                string val = map.Get("mykey");
                Assert.Null(val);
            }
        }

        public class PopulatedMap
        {

            readonly XAttributeNameMap map;

            public PopulatedMap()
            {
                map = new XAttributeNameMap("MyMap");
                map.RegisterMapping("key", "value");
            }

            [Fact]
            public void TestToString()
            {
                string mapstr = map.ToString();
                Assert.Matches("Attribute name map: MyMap\\nkey -> value", mapstr);
            }

            [Fact]
            public void TestRegisterDuplicateMapping()
            {
                map.RegisterMapping("key", "value");
                // should not fail
                Assert.Single(map);
                Assert.True(map.ContainsKey("key"));
                Assert.True(map.ContainsValue("value"));
            }

            [Fact]
            public void GettingMapping()
            {
                string val = map.Get("key");
                Assert.Equal("value", val);
            }
        }
    }
}