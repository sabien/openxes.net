﻿using Xunit;
using Xunit.Abstractions;
using OpenXesNet.model;
using System.IO;

namespace OpenXesNet.io
{
    public class XesXMLParserTest
    {

        ITestOutputHelper output;

        public XesXMLParserTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void CreateParser()
        {
            using (XesXmlParser parser = new XesXmlParser())
            {
                Assert.NotNull(parser);
                Assert.Equal("XES XML", parser.Name);
            }
        }

        [Fact]
        public void ParseBufferedStream()
        {
            XesXmlParser parser;
            using (BufferedStream bs = new BufferedStream(GenerateStreamFromString(sampleXesXml)))
            {
                using (parser = new XesXmlParser())
                {
                    IXLog log = parser.Parse(bs);

                    Assert.NotNull(log);
                }
            }
        }

        [Collection("LoadTest")]
        public class LoadTests
        {

            [Fact(Skip = "Time consuming test")]
            public void ParseFromFile()
            {
                string fileName = Path.Combine("/", "Users", "almarro1", "MEGAsync", "Tesis", "Datasets", "BPI Challenge 2017", "BPI Challenge 2017.xes");
                using (XesXmlParser parser = new XesXmlParser())
                {
                    XLog log = (XLog)parser.Parse(fileName);
                    Assert.NotNull(log);
                }
            }
        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        protected string sampleXesXml = "" +
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<log xes.version=\"2.0\" xes.features=\"arbitrary-depth\" xmlns=\"http://www.xes-standard.org/\">" +
                "<extension  name=\"Concept\" prefix=\"concept\" uri=\"http://www.xes-standard.org/concept.xesext\"/>" +
                "<extension name=\"Time\" prefix=\"time\" uri=\"http://www.xes-standard.org/time.xesext\"/>" +
                "<global scope=\"trace\">" +
                    "<string key=\"concept:name\" value=\"\"/>" +
                "</global>" +
                "<global scope=\"event\">" +
                    "<string key=\"concept:name\" value=\"\"/>" +
                    "<date key=\"time:timestamp\" value=\"1970-01-01T00:00:00.000+00:00\"/>" +
                    "<string key=\"system\" value=\"\"/>" +
                "</global>" +
                "<classifier name=\"Activity\" keys=\"concept:name\"/>" +
                "<classifier name=\"Another\" keys=\"concept:name system\"/>" +
                "<float key=\"log attribute\" value=\"2335.23\"/>" +
                "<trace>" +
                    "<string key=\"concept:name\" value=\"Trace number one\"/>" +
                    "<event>" +
                        "<string key=\"concept:name\" value=\"Register client\"/>" +
                        "<string key=\"system\" value=\"alpha\"/>" +
                        "<date key=\"time:timestamp\" value=\"2009-11-25T14:12:45:000+02:00\"/>" +
                        "<int key=\"attempt\" value=\"23\">" +
                            "<boolean key=\"tried hard\" value=\"false\"/>" +
                        "</int>" +
                    "</event>" +
                    "<event>" +
                        "<string key=\"concept:name\" value=\"Mail rejection\"/>" +
                        "<string key=\"system\" value=\"beta\"/>" +
                        "<date key=\"time:timestamp\" value=\"2009-11-28T11:18:45:000+02:00\"/>" +
                    "</event>" +
                "</trace>" +
            "</log>";
    }

}
