﻿using Xunit;
using OpenXesNet.model;
using System.IO;
using OpenXesNet.extension.std;
using OpenXesNet.info;

namespace OpenXesNet.io
{
    public class XesXMLGzipParserTest
    {
        readonly FileInfo fInfo1 = new FileInfo(Path.Combine(".", "assets", "LevelD2.xes.gz"));
        readonly FileInfo fInfo2 = new FileInfo(Path.Combine(".", "assets", "LevelD2.xes"));

        [Fact]
        public void CanParseTest()
        {
            using (XesXmlGzipParser parser = new XesXmlGzipParser())
            {
                Assert.True(parser.CanParse(fInfo1));
                Assert.False(parser.CanParse(fInfo2));
            }
        }

        [Fact]
        public void ParseFromFile()
        {
            using (XesXmlGzipParser parser = new XesXmlGzipParser())
            {
                XLog log = (XLog)parser.Parse(fInfo1.FullName);
                Assert.NotNull(log);

                // Check log version and feature
                Assert.Equal("1849.2016", log.Version);
                Assert.Equal("", log.Features);

                // Check extesions were loaded correctly
                Assert.NotNull(log.Extensions);
                Assert.NotEmpty(log.Extensions);
                Assert.Equal(4, log.Extensions.Count);
                Assert.Contains(XLifecycleExtension.Instance, log.Extensions);
                Assert.Contains(XOrganizationalExtension.Instance, log.Extensions);
                Assert.Contains(XConceptExtension.Instance, log.Extensions);
                Assert.Contains(XTimeExtension.Instance, log.Extensions);

                // Check global attributes
                Assert.NotNull(log.GlobalTraceAttributes);
                Assert.Single(log.GlobalTraceAttributes);
                Assert.True(log.GlobalTraceAttributes.Exists((item) => item.Key == "concept:name"));

                Assert.NotNull(log.GlobalEventAttributes);
                Assert.Equal(6, log.GlobalEventAttributes.Count);
                Assert.True(log.GlobalEventAttributes.Exists((item) => item.Key == "concept:name"));
                Assert.True(log.GlobalEventAttributes.Exists((item) => item.Key == "concept:instance"));
                Assert.True(log.GlobalEventAttributes.Exists((item) => item.Key == "lifecycle:transition"));
                Assert.True(log.GlobalEventAttributes.Exists((item) => item.Key == "org:resource"));
                Assert.True(log.GlobalEventAttributes.Exists((item) => item.Key == "org:group"));
                Assert.True(log.GlobalEventAttributes.Exists((item) => item.Key == "time:timestamp"));

                // Check classifiers
                Assert.NotNull(log.Classifiers);
                Assert.Single(log.Classifiers);
                Assert.True(log.Classifiers.Exists((item) => item.Name == "(Name AND Transition)"));

                // Check log attributes
                Assert.NotNull(log.GetAttributes());
                Assert.Single(log.GetAttributes());
                Assert.Contains("concept:name", log.GetAttributes().Keys);

                // Check traces
                Assert.NotEmpty(log);
                Assert.Equal(1104, log.Count); // 1104 cases

                IXLogInfo info = XLogInfoFactory.CreateLogInfo(log, log.Classifiers[0]);
                Assert.NotNull(info);

                Assert.Equal(1104, info.NumberOfTraces);
                Assert.Equal(11855, info.NumberOfEvents);
                Assert.Equal(12, info.GetEventClasses().Count);
            }
        }
    }
}
