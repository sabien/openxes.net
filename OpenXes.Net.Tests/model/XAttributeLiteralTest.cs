﻿using Xunit;
using Xunit.Abstractions;
using OpenXesNet.extension;
using System;

namespace OpenXesNet.model
{

    public class XAttributeLiteralTest
    {
        readonly XExtension ext = new XExtension("name", "prefix", new UriBuilder("www.example.es").Uri);
        ITestOutputHelper output;

        public XAttributeLiteralTest(ITestOutputHelper output){
            this.output = output;
            output.WriteLine("**** Hola");
        }

        [Fact]
        public void CreateWithoutExtension()
        {
            XAttributeLiteral strAttr = new XAttributeLiteral("key", "value");
            Assert.NotNull(strAttr);
            Assert.Equal("key", strAttr.Key);
            Assert.Equal("value", strAttr.Value);
            Assert.Null(strAttr.Extension);
        }

        [Fact]
        public void CreateWithExtension()
        {
            XExtension ext2 = new XExtension("name", "prefix", new UriBuilder("www.example.es").Uri);
            XAttributeLiteral strAttr = new XAttributeLiteral("key", "value", ext2);
            Assert.NotNull(strAttr);
            Assert.Equal("key", strAttr.Key);
            Assert.Equal("value", strAttr.Value);
            Assert.NotNull(strAttr.Extension);
            Assert.Same(strAttr.Extension, ext2);
        }

        [Fact]
        public void ModifyValue()
        {
            XAttributeLiteral strAttr = new XAttributeLiteral("key", "value");
            Assert.Equal("value", strAttr.Value);

            strAttr.Value ="new value";
            Assert.Equal("new value", strAttr.Value);
        }

        [Fact]
        public void ToStringShoulReturnTheValue()
        {
            XAttributeLiteral strAttr = new XAttributeLiteral("key", "value");
            Assert.Equal("value", strAttr.ToString());
        }

        [Fact]
        public void CreatingAClone()
        {
            XAttributeLiteral strAttr = new XAttributeLiteral("key", "value", ext);
            XAttributeLiteral strAttrClone = (XAttributeLiteral)strAttr.Clone();

            Assert.NotNull(strAttrClone);
            Assert.NotSame(strAttr, strAttrClone);

            Assert.Equal(strAttrClone.Key, strAttr.Key);
            Assert.NotSame(strAttrClone.Value, strAttr.Value);
            Assert.Equal(strAttrClone.Value, strAttr.Value);
            Assert.Same(strAttr.Extension, strAttrClone.Extension);
        }
    }
}