﻿using System;
using Xunit;
using OpenXesNet.factory;

namespace OpenXesNet.model
{
    public class XAttributeTimestampTest
    {
        IXFactory factory;
        public XAttributeTimestampTest()
        {
            factory = new XFactoryNaive();
        }

        [Fact]
        public void ToStringTest(){
            IXAttributeTimestamp ts = factory.CreateAttributeTimestamp("testDate", new DateTime(2017,10,24,22,45,12,567), null);
            string formatted = ts.ToString();
            Assert.NotNull(formatted);
        }

        [Fact]
        public void ParseString(){
            string val = "2009-11-28T11:18:45:000+02:00";
            DateTime result = XAttributeTimestamp.Parse(val);
           
            Assert.Equal(2009, result.Year);
            Assert.Equal(11, result.Month);
            Assert.Equal(9, result.Hour); // 11 - 2 hours
        }

    }
}
