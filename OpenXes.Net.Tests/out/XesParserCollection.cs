﻿using System;
using Xunit;
using OpenXesNet.model;
using System.IO;
using OpenXesNet.logging;

namespace OpenXesNet.io
{
    [CollectionDefinition("Xes parser collection")]
    public class XesParserCollection : ICollectionFixture<XesSerializerFixture>
    {
    }

    public class XesSerializerFixture : IDisposable
    {
        public readonly IXLog log;
        public readonly DirectoryInfo testTempDir;

        public XesSerializerFixture()
        {
            XLogging.AddListener(new XStdOutLoggingListener(), XLogging.Importance.DEBUG);
            using (XesXmlParser parser = new XesXmlParser())
            {
                log = parser.Parse(Path.Combine(".", "assets", "LevelD2.xes"));
            }
            // Create the tempdir where to put files
            testTempDir = Directory.CreateDirectory(Path.Combine(".", "temptTestData"));
        }

        public void Dispose()
        {
            // Clean temp files
            this.testTempDir.Delete(true);
        }
    }
}
