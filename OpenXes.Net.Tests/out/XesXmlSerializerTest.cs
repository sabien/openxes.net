﻿using System.IO;
using Xunit;

namespace OpenXesNet.io
{
    [Collection("Xes parser collection")]
    public class XesXmlSerializerTest
    {
        readonly XesSerializerFixture logFixture;

        public XesXmlSerializerTest(XesSerializerFixture fixture)
        {
            this.logFixture = fixture;

        }

        [Fact]
        public void SerializeToStream()
        {
            XesXmlSerializer serializer = new XesXmlSerializer();
            MemoryStream memStream = new MemoryStream();

            serializer.Serialize(logFixture.log, memStream);

            Assert.True(memStream.Length > 0);
        }

        [Fact]
        public void SerializeToFile()
        {
            XesXmlSerializer serializer = new XesXmlSerializer();

            string fileName = Path.Combine(logFixture.testTempDir.ToString(), "outFile.xes");
            FileStream fStream = File.Create(fileName);

            serializer.Serialize(logFixture.log, fStream);

            FileInfo outInfo = new FileInfo(fileName);
            Assert.True(outInfo.Exists);
            Assert.True(outInfo.Length > 0);
        }
    }
}