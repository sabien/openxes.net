﻿using Xunit;
using System.IO;

namespace OpenXesNet.io
{
    [Collection("Xes parser collection")]
    public class XesXmlGZIPSerializerTest
    {
        readonly XesSerializerFixture fixture;

        public XesXmlGZIPSerializerTest(XesSerializerFixture fixture)
        {
            this.fixture = fixture;
        }

        [Fact]
        public void SerializeToStream()
        {
            XesXmlGZIPSerializer serializer = new XesXmlGZIPSerializer();
            using (MemoryStream memStream = new MemoryStream())
            {
                serializer.Serialize(fixture.log, memStream);
                // Because Serialize() will automatically close the stream, we need to create a new one
                // with the contents of the original to run assertions
                using (MemoryStream ms2 = new MemoryStream(memStream.ToArray()))
                {
                    Assert.True(ms2.Length > 0);
                }
            }
        }

        [Fact]
        public void SerializeToFile()
        {
            XesXmlGZIPSerializer serializer = new XesXmlGZIPSerializer();

            string fileName = Path.Combine(fixture.testTempDir.ToString(), "outFile.xes.gz");
            FileStream fStream = File.Create(fileName);

            serializer.Serialize(fixture.log, fStream);

            FileInfo outInfo = new FileInfo(fileName);
            Assert.True(outInfo.Exists);
            Assert.True(outInfo.Length > 0);
        }
    }
}
