﻿using System;
using Xunit;
using Xunit.Abstractions;
using System.IO;

namespace OpenXesNet.extension
{
    public class XExtensionParserTest
    {
        ITestOutputHelper output;

        public XExtensionParserTest(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void ParseWellFormedExtensionFromFile()
        {
            FileInfo f = new FileInfo(Path.Combine(".", "assets", "identityExtension.xesext"));
            XExtension ext = XExtensionParser.Instance.Parse(f);

            Assert.NotNull(ext);
            Assert.Equal("identity", ext.Prefix);
            Assert.Equal("Identity", ext.Name);

            Assert.Single(ext.LogAttributes);
            Assert.True(ext.LogAttributes.ContainsKey("id"));
            Assert.Single(ext.TraceAttributes);
            Assert.True(ext.TraceAttributes.ContainsKey("id"));
            Assert.Single(ext.MetaAttributes);
            Assert.True(ext.MetaAttributes.ContainsKey("id"));
            Assert.Single(ext.EventAttributes);
            Assert.True(ext.EventAttributes.ContainsKey("id"));

        }

        [Fact]
        public void ParseWellFormedExtensionFromUri()
        {
            Uri uri = new UriBuilder("http://www.xes-standard.org/identity.xesext").Uri;
            XExtension ext = XExtensionParser.Instance.Parse(uri);

            Assert.NotNull(ext);
            Assert.Equal("identity", ext.Prefix);
            Assert.Equal("Identity", ext.Name);

            Assert.Single(ext.LogAttributes);
            Assert.True(ext.LogAttributes.ContainsKey("id"));
            Assert.Single(ext.TraceAttributes);
            Assert.True(ext.TraceAttributes.ContainsKey("id"));
            Assert.Single(ext.MetaAttributes);
            Assert.True(ext.MetaAttributes.ContainsKey("id"));
            Assert.Single(ext.EventAttributes);
            Assert.True(ext.EventAttributes.ContainsKey("id"));
        }
    }
}
