﻿using System;
using Xunit;
using OpenXesNet.extension;

namespace OpenXesNet.extension
{
    public class XExtensionManagerTest
    {
        [Fact]
        public void GettingInstance(){
            XExtensionManager manager = XExtensionManager.Instance;
            Assert.NotNull(manager);
        }
    }
}
