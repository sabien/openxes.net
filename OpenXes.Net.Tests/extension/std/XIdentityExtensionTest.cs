﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;
using OpenXesNet.id;
using OpenXesNet.factory;
using OpenXesNet.model;
using System.Linq;

namespace OpenXesNet.extension.std
{
    public class XIdentityExtensionTest
    {
        readonly XIdentityExtension instance;
        ITestOutputHelper output;

        public XIdentityExtensionTest(ITestOutputHelper output)
        {
            this.output = output;
            instance = XIdentityExtension.Instance;
            Assert.NotNull(instance);
        }

        [Fact]
        public void GetInstance()
        {
            XConceptExtension instance2 = XConceptExtension.Instance;
            Assert.NotNull(instance2);
            Assert.Equal("identity", instance.Prefix);
            Assert.Equal("Identity", instance.Name);
        }

        [Fact]
        public void LogAttributes()
        {
            Dictionary<string, XAttribute> logAttr = instance.LogAttributes;
            Assert.NotNull((logAttr));
            string[] attrs = { "id" };

            Assert.True(attrs.Length == logAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(logAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void TraceAttributes()
        {
            Dictionary<string, XAttribute> traceAttr = instance.TraceAttributes;
            Assert.NotNull((traceAttr));
            string[] attrs = { "id" };

            Assert.True(attrs.Length == traceAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(traceAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void EventAttributes()
        {
            Dictionary<string, XAttribute> evtAttr = instance.EventAttributes;
            Assert.NotNull((evtAttr));
            string[] attrs = { "id" };

            Assert.True(attrs.Length == evtAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(evtAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void MetaAttributes()
        {
            Dictionary<string, XAttribute> metaAttr = instance.MetaAttributes;
            Assert.NotNull((metaAttr));
            string[] attrs = { "id" };

            Assert.True(attrs.Length == metaAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(metaAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void IdHelpers()
        {
            XLog log = XFactoryRegistry.Instance.CurrentDefault.CreateLog();
            XTrace trace = XFactoryRegistry.Instance.CurrentDefault.CreateTrace();
            XEvent evt = XFactoryRegistry.Instance.CurrentDefault.CreateEvent();
            XAttribute attr = XFactoryRegistry.Instance.CurrentDefault.CreateAttributeLiteral("aaa", "bb");
            XID id = new XID();
            Assert.Null(instance.ExtractID(log));
            Assert.Null(instance.ExtractID(trace));
            Assert.Null(instance.ExtractID(evt));
            Assert.Null(instance.ExtractID((IXAttributable)attr));

            instance.AssignID(log, id);
            instance.AssignID(trace, id);
            instance.AssignID(evt, id);
            instance.AssignID((IXAttributable)attr, id);

            XID id2 = instance.ExtractID(log);
            Assert.NotNull(id2);
            Assert.Equal(id, id2);
            id2 = instance.ExtractID(trace);
            Assert.NotNull(id2);
            Assert.Equal(id, id2);
            id2 = instance.ExtractID(evt);
            Assert.NotNull(id2);
            Assert.Equal(id, id2);
            id2 = instance.ExtractID((IXAttributable)attr);
            Assert.NotNull(id2);
            Assert.Equal(id, id2);
        }
    }
}
