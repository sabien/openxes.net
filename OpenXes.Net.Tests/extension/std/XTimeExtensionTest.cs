﻿using System;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;
using OpenXesNet.factory;
using OpenXesNet.model;
using System.Linq;

namespace OpenXesNet.extension.std
{
    public class XTimeExtensionTest
    {
        readonly XTimeExtension instance;
        ITestOutputHelper output;

        public XTimeExtensionTest(ITestOutputHelper output)
        {
            this.output = output;
            instance = XTimeExtension.Instance;
            Assert.NotNull(instance);
        }

        [Fact]
        public void GetInstance()
        {
            XConceptExtension instance2 = XConceptExtension.Instance;
            Assert.NotNull(instance2);
            Assert.Equal("time", instance.Prefix);
            Assert.Equal("Time", instance.Name);
        }

        [Fact]
        public void LogAttributes()
        {
            Dictionary<string, XAttribute> logAttr = instance.LogAttributes;
            Assert.NotNull((logAttr));
            string[] attrs = { };

            Assert.True(attrs.Length == logAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(logAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void TraceAttributes()
        {
            Dictionary<string, XAttribute> traceAttr = instance.TraceAttributes;
            Assert.NotNull((traceAttr));
            string[] attrs = { };

            Assert.True(attrs.Length == traceAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(traceAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void EventAttributes()
        {
            Dictionary<string, XAttribute> evtAttr = instance.EventAttributes;
            Assert.NotNull((evtAttr));
            string[] attrs = { "timestamp" };

            Assert.True(attrs.Length == evtAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(evtAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void MetaAttributes()
        {
            Dictionary<string, XAttribute> metaAttr = instance.MetaAttributes;
            Assert.NotNull((metaAttr));
            string[] attrs = { };

            Assert.True(attrs.Length == metaAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(metaAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void TimestampHelpers()
        {
            DateTime dt = DateTime.Now;
            XEvent evt = XFactoryRegistry.Instance.CurrentDefault.CreateEvent();

            Assert.Null(instance.ExtractTimestamp(evt));

            instance.AssignTimestamp(evt, dt);

            DateTime? dt2 = instance.ExtractTimestamp(evt);
            Assert.NotNull(dt2);
            Assert.Equal(dt, dt2);

        }
    }
}
