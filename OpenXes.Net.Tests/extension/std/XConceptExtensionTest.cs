﻿using System;
using System.Linq;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;
using OpenXesNet.extension.std;
using OpenXesNet.model;
using OpenXesNet.factory;

namespace OpenXesNet.extension.std
{
    public class XConceptExtensionTest
    {
        readonly XConceptExtension instance;
        ITestOutputHelper output;

        public XConceptExtensionTest(ITestOutputHelper output)
        {
            this.output = output;
            instance = XConceptExtension.Instance;
            Assert.NotNull(instance);
        }

        [Fact]
        public void GetInstance()
        {
            XConceptExtension instance2 = XConceptExtension.Instance;
            Assert.NotNull(instance2);
            Assert.Equal("concept", instance.Prefix);
            Assert.Equal("Concept", instance.Name);
        }

        [Fact]
        public void LogAttributes()
        {
            Dictionary<string, XAttribute> logAttr = instance.LogAttributes;
            Assert.NotNull((logAttr));
            string[] attrs = { "name" };

            Assert.True(attrs.Length == logAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(logAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void TraceAttributes()
        {
            Dictionary<string, XAttribute> traceAttr = instance.TraceAttributes;
            Assert.NotNull((traceAttr));
            string[] attrs = { "name" };

            Assert.True(attrs.Length == traceAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(traceAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void EventAttributes()
        {
            Dictionary<string, XAttribute> evtAttr = instance.EventAttributes;
            Assert.NotNull((evtAttr));
            string[] attrs = { "name", "instance" };

            Assert.True(attrs.Length == evtAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(evtAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void MetaAttributes()
        {
            Dictionary<string, XAttribute> metaAttr = instance.MetaAttributes;
            Assert.NotNull((metaAttr));
            string[] attrs = { };

            Assert.True(attrs.Length == metaAttr.Count);

            foreach (string attr in attrs)
            {
                Assert.True(metaAttr.ContainsKey(attr));
            }
        }

        [Fact]
        public void NameHelpers()
        {
            XLog log = XFactoryRegistry.Instance.CurrentDefault.CreateLog();
            XTrace trace = XFactoryRegistry.Instance.CurrentDefault.CreateTrace();
            XEvent evt = XFactoryRegistry.Instance.CurrentDefault.CreateEvent();

            Assert.Null(instance.ExtractName(log));
            Assert.Null(instance.ExtractName(trace));
            Assert.Null(instance.ExtractName(evt));

            instance.AssignName(log, "logName");
            instance.AssignName(trace, "traceName");
            instance.AssignName(evt, "eventName");

            string name = instance.ExtractName(log);
            Assert.NotNull(name);
            Assert.Equal("logName", name);
            name = instance.ExtractName(trace);
            Assert.NotNull(name);
            Assert.Equal("traceName", name);
            name = instance.ExtractName(evt);
            Assert.NotNull(name);
            Assert.Equal("eventName", name);
        }

        [Fact]
        public void InstanceHelpers()
        {
            XEvent evt = XFactoryRegistry.Instance.CurrentDefault.CreateEvent();

            Assert.Null(instance.ExtractInstance(evt));

            instance.AssignName(evt, "eventInstance");

            string inst = instance.ExtractName(evt);
            Assert.NotNull(inst);
            Assert.Equal("eventInstance", inst);
        }
    }
}
