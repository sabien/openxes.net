var classOpenXesNet_1_1info_1_1XAttributeInfo =
[
    [ "XAttributeInfo", "classOpenXesNet_1_1info_1_1XAttributeInfo_a98f33c6a0413e8424c0f0cbb9859927b.html#a98f33c6a0413e8424c0f0cbb9859927b", null ],
    [ "GetAttributeKeys", "classOpenXesNet_1_1info_1_1XAttributeInfo_af77efa1971d3405c34f01c9f20459815.html#af77efa1971d3405c34f01c9f20459815", null ],
    [ "GetAttributes", "classOpenXesNet_1_1info_1_1XAttributeInfo_af52b890d2c5900869a849a2933e356d1.html#af52b890d2c5900869a849a2933e356d1", null ],
    [ "GetAttributesForExtension", "classOpenXesNet_1_1info_1_1XAttributeInfo_a863e2eb9b270ccde6f60fc0305a1cf3a.html#a863e2eb9b270ccde6f60fc0305a1cf3a", null ],
    [ "GetAttributesForType", "classOpenXesNet_1_1info_1_1XAttributeInfo_a6ab93d646dd9574842ae64ca8b312e64.html#a6ab93d646dd9574842ae64ca8b312e64", null ],
    [ "GetAttributesWithoutExtension", "classOpenXesNet_1_1info_1_1XAttributeInfo_a70654f10c820132c4d454c7d1fe85d7f.html#a70654f10c820132c4d454c7d1fe85d7f", null ],
    [ "GetFrequency", "classOpenXesNet_1_1info_1_1XAttributeInfo_a37c1b604d06d445a3c7373593923545f.html#a37c1b604d06d445a3c7373593923545f", null ],
    [ "GetFrequency", "classOpenXesNet_1_1info_1_1XAttributeInfo_ade149e1c3a3546ef0e74d1079f237ede.html#ade149e1c3a3546ef0e74d1079f237ede", null ],
    [ "GetKeysForExtension", "classOpenXesNet_1_1info_1_1XAttributeInfo_a87b8083df4b0252bb9b38b08a3dc29aa.html#a87b8083df4b0252bb9b38b08a3dc29aa", null ],
    [ "GetKeysForType", "classOpenXesNet_1_1info_1_1XAttributeInfo_a0f92e3fbfb5871321a3656395cade1c7.html#a0f92e3fbfb5871321a3656395cade1c7", null ],
    [ "GetKeysWithoutExtension", "classOpenXesNet_1_1info_1_1XAttributeInfo_a3ba455f1f684203b72b450a371bd29a9.html#a3ba455f1f684203b72b450a371bd29a9", null ],
    [ "GetRelativeFrequency", "classOpenXesNet_1_1info_1_1XAttributeInfo_a667f712bf672c456314e605acaa7e558.html#a667f712bf672c456314e605acaa7e558", null ],
    [ "GetRelativeFrequency", "classOpenXesNet_1_1info_1_1XAttributeInfo_aabfe06b52df43bcd7b8d707a71a2bd4d.html#aabfe06b52df43bcd7b8d707a71a2bd4d", null ],
    [ "Register", "classOpenXesNet_1_1info_1_1XAttributeInfo_a23c40b4012ff5d261d8def1d640d4e2f.html#a23c40b4012ff5d261d8def1d640d4e2f", null ]
];