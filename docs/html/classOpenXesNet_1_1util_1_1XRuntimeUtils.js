var classOpenXesNet_1_1util_1_1XRuntimeUtils =
[
    [ "OS", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24", [
      [ "WIN32", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24a160ad810c464a220e4c779edbd6aedc1", null ],
      [ "MACOSX", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24a2083dff597acf7e790a8ab8b72194e66", null ],
      [ "MACOSCLASSIC", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24a1cffb95d7ae88c994eb9e1d2f6b88852", null ],
      [ "LINUX", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24aee86602a23e7a9a5136f6b2138894aa5", null ],
      [ "BSD", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24ac539e03be7b269c8a84f85437bc8d298", null ],
      [ "RISCOS", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24aff15cfd9a5168d9aa07b7d80d40014c5", null ],
      [ "BEOS", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24a738b6fe65e2e682c0038bbab08aa1e2e", null ],
      [ "UNKNOWN", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ad38c8e4b9dd6ba58aa3269abc71bdd24.html#ad38c8e4b9dd6ba58aa3269abc71bdd24a696b031073e74bf2cb98e5ef201d4aa3", null ]
    ] ],
    [ "DetermineOS", "classOpenXesNet_1_1util_1_1XRuntimeUtils_a058f71d2e6ff254f4c2fe4ad2dd018c2.html#a058f71d2e6ff254f4c2fe4ad2dd018c2", null ],
    [ "GetExtensionCacheFolder", "classOpenXesNet_1_1util_1_1XRuntimeUtils_aba34da83ce32d244890b1e1852d8a0e2.html#aba34da83ce32d244890b1e1852d8a0e2", null ],
    [ "GetSupportFolder", "classOpenXesNet_1_1util_1_1XRuntimeUtils_a82a6f9b0f93a84dfbc09f4124a0c2b8d.html#a82a6f9b0f93a84dfbc09f4124a0c2b8d", null ],
    [ "IsRunningLinux", "classOpenXesNet_1_1util_1_1XRuntimeUtils_aa821851d269b91b1f00ed98467ee9ac1.html#aa821851d269b91b1f00ed98467ee9ac1", null ],
    [ "IsRunningMacOsX", "classOpenXesNet_1_1util_1_1XRuntimeUtils_a12bb67aa9ae5fc1e2e500871805eaa64.html#a12bb67aa9ae5fc1e2e500871805eaa64", null ],
    [ "IsRunningUnix", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ac2a55f4f082d38ef4dd04097bf562f3f.html#ac2a55f4f082d38ef4dd04097bf562f3f", null ],
    [ "IsRunningWindows", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ae2bee5d99d8b6f05e2d3f1ece1597b2b.html#ae2bee5d99d8b6f05e2d3f1ece1597b2b", null ],
    [ "currentOs", "classOpenXesNet_1_1util_1_1XRuntimeUtils_aa5b67e1bdfcdb7fefecdd03a73d9f81d.html#aa5b67e1bdfcdb7fefecdd03a73d9f81d", null ],
    [ "OPENXES_VERSION", "classOpenXesNet_1_1util_1_1XRuntimeUtils_ab218d33fe020713e2ab233aeeb2f7d4f.html#ab218d33fe020713e2ab233aeeb2f7d4f", null ],
    [ "XES_VERSION", "classOpenXesNet_1_1util_1_1XRuntimeUtils_aa3276e7c742de123bfb3ba4d6f0f950b.html#aa3276e7c742de123bfb3ba4d6f0f950b", null ]
];