var classOpenXesNet_1_1model_1_1XEvent =
[
    [ "XEvent", "classOpenXesNet_1_1model_1_1XEvent_a3a9557c3a9de6a6293de0a9c4c229256.html#a3a9557c3a9de6a6293de0a9c4c229256", null ],
    [ "XEvent", "classOpenXesNet_1_1model_1_1XEvent_aaf38ff0eb1e201f305e3ee13b6e73a60.html#aaf38ff0eb1e201f305e3ee13b6e73a60", null ],
    [ "XEvent", "classOpenXesNet_1_1model_1_1XEvent_a447254c57e6f10aff3459d1bca40cde1.html#a447254c57e6f10aff3459d1bca40cde1", null ],
    [ "XEvent", "classOpenXesNet_1_1model_1_1XEvent_ac3fe8e9c3dd81720786ca2315fc3a191.html#ac3fe8e9c3dd81720786ca2315fc3a191", null ],
    [ "Accept", "classOpenXesNet_1_1model_1_1XEvent_afaf43fa65948eccc0a9b1e9504262dc1.html#afaf43fa65948eccc0a9b1e9504262dc1", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XEvent_a078741166821a55b34a7a117d28c421d.html#a078741166821a55b34a7a117d28c421d", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XEvent_a333fa8c6678b466471cb9fb74af3e20b.html#a333fa8c6678b466471cb9fb74af3e20b", null ],
    [ "GetAttributes", "classOpenXesNet_1_1model_1_1XEvent_a797d85cc7bcb359308e95362ca88ddc2.html#a797d85cc7bcb359308e95362ca88ddc2", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XEvent_a3cf82df643ceb59e7a40c0bd45f25ff3.html#a3cf82df643ceb59e7a40c0bd45f25ff3", null ],
    [ "HasAttributes", "classOpenXesNet_1_1model_1_1XEvent_a5c04d0cf85d9ca54d4a4bd50f1619b4a.html#a5c04d0cf85d9ca54d4a4bd50f1619b4a", null ],
    [ "SetAttributes", "classOpenXesNet_1_1model_1_1XEvent_adfd864c75cbf164da6e196a6831aa684.html#adfd864c75cbf164da6e196a6831aa684", null ],
    [ "Extensions", "classOpenXesNet_1_1model_1_1XEvent_a87c65a7500a964bd8b2af8fe9d7dfb27.html#a87c65a7500a964bd8b2af8fe9d7dfb27", null ],
    [ "Id", "classOpenXesNet_1_1model_1_1XEvent_adbe6fb2764ee06dd83489cdc3c378a13.html#adbe6fb2764ee06dd83489cdc3c378a13", null ]
];