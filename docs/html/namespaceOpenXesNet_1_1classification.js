var namespaceOpenXesNet_1_1classification =
[
    [ "IXEventClassifier", "interfaceOpenXesNet_1_1classification_1_1IXEventClassifier.html", "interfaceOpenXesNet_1_1classification_1_1IXEventClassifier" ],
    [ "XEventAndClassifier", "classOpenXesNet_1_1classification_1_1XEventAndClassifier.html", "classOpenXesNet_1_1classification_1_1XEventAndClassifier" ],
    [ "XEventAttributeClassifier", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier.html", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier" ],
    [ "XEventClass", "classOpenXesNet_1_1classification_1_1XEventClass.html", "classOpenXesNet_1_1classification_1_1XEventClass" ],
    [ "XEventClasses", "classOpenXesNet_1_1classification_1_1XEventClasses.html", "classOpenXesNet_1_1classification_1_1XEventClasses" ],
    [ "XEventLifeTransClassifier", "classOpenXesNet_1_1classification_1_1XEventLifeTransClassifier.html", "classOpenXesNet_1_1classification_1_1XEventLifeTransClassifier" ],
    [ "XEventNameClassifier", "classOpenXesNet_1_1classification_1_1XEventNameClassifier.html", "classOpenXesNet_1_1classification_1_1XEventNameClassifier" ],
    [ "XEventResourceClassifier", "classOpenXesNet_1_1classification_1_1XEventResourceClassifier.html", "classOpenXesNet_1_1classification_1_1XEventResourceClassifier" ]
];