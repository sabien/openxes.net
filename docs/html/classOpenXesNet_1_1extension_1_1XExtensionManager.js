var classOpenXesNet_1_1extension_1_1XExtensionManager =
[
    [ "CacheExtensionAsync", "classOpenXesNet_1_1extension_1_1XExtensionManager_ade2d8016d87b4f1b0c0a25bb9548962e.html#ade2d8016d87b4f1b0c0a25bb9548962e", null ],
    [ "GetByIndex", "classOpenXesNet_1_1extension_1_1XExtensionManager_a78b1b9c893d548bf902bf60231a2c82e.html#a78b1b9c893d548bf902bf60231a2c82e", null ],
    [ "GetByName", "classOpenXesNet_1_1extension_1_1XExtensionManager_a9d3496e200d48f10adf31c34a7a30ae9.html#a9d3496e200d48f10adf31c34a7a30ae9", null ],
    [ "GetByPrefix", "classOpenXesNet_1_1extension_1_1XExtensionManager_accf3b3460002accea2d97cce10cf6375.html#accf3b3460002accea2d97cce10cf6375", null ],
    [ "GetByUri", "classOpenXesNet_1_1extension_1_1XExtensionManager_a6d3c762d96e639d33e2a979e570c143b.html#a6d3c762d96e639d33e2a979e570c143b", null ],
    [ "GetIndex", "classOpenXesNet_1_1extension_1_1XExtensionManager_a06fb1d377bc50c437d9d4476f7d2f080.html#a06fb1d377bc50c437d9d4476f7d2f080", null ],
    [ "LoadExtensionCache", "classOpenXesNet_1_1extension_1_1XExtensionManager_a764b8d86109f1a7790c78625aec53067.html#a764b8d86109f1a7790c78625aec53067", null ],
    [ "Register", "classOpenXesNet_1_1extension_1_1XExtensionManager_acfaeb44f5c9fdd95f69deb0be7688e7c.html#acfaeb44f5c9fdd95f69deb0be7688e7c", null ],
    [ "RegisterStandardExtensions", "classOpenXesNet_1_1extension_1_1XExtensionManager_ad946f98a8915eb656c8dc8e20a81f561.html#ad946f98a8915eb656c8dc8e20a81f561", null ],
    [ "Instance", "classOpenXesNet_1_1extension_1_1XExtensionManager_a980adb6b382c349c4f605b94c307e1d2.html#a980adb6b382c349c4f605b94c307e1d2", null ]
];