var classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension =
[
    [ "AssignLength", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_ad48d296dc5ce78b8134e03e473eef471.html#ad48d296dc5ce78b8134e03e473eef471", null ],
    [ "AssignLevel", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a2bbb366d8132bfdc49c166dc85def166.html#a2bbb366d8132bfdc49c166dc85def166", null ],
    [ "AssignParentId", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_af099f3bf6a861d46dd2e9fb60ede66e7.html#af099f3bf6a861d46dd2e9fb60ede66e7", null ],
    [ "ExtractLength", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a16961d82721640e05229f74e38f3dd98.html#a16961d82721640e05229f74e38f3dd98", null ],
    [ "ExtractLevel", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_aa4bde30e5a6c7adc3b1d2443562eb27f.html#aa4bde30e5a6c7adc3b1d2443562eb27f", null ],
    [ "ExtractParentId", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_aaf65988336a3c07037636f686afc091a.html#aaf65988336a3c07037636f686afc091a", null ],
    [ "RemoveLength", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a01981dd17b00a26545d76035fe7df03c.html#a01981dd17b00a26545d76035fe7df03c", null ],
    [ "RemoveLevel", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a43c74fd8143c536ceb774f1ec4d31de4.html#a43c74fd8143c536ceb774f1ec4d31de4", null ],
    [ "RemoveParentId", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a615111085cd73c5518a44c7bb1485b2b.html#a615111085cd73c5518a44c7bb1485b2b", null ],
    [ "ATTR_LENGTH", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_ac29454985c26d80011f4d2b0d91c6a92.html#ac29454985c26d80011f4d2b0d91c6a92", null ],
    [ "ATTR_LEVEL", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_ad905a5ba017853e28fb11c2a7638c907.html#ad905a5ba017853e28fb11c2a7638c907", null ],
    [ "ATTR_PID", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a64ff0266288ffe3d00f3c5805678e393.html#a64ff0266288ffe3d00f3c5805678e393", null ],
    [ "EXTENSION_URI", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_af0a75157635fbabb675a2b6cb915c33a.html#af0a75157635fbabb675a2b6cb915c33a", null ],
    [ "KEY_LENGTH", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a59d4f9aeec80d41d7a5241a3d48ba9b8.html#a59d4f9aeec80d41d7a5241a3d48ba9b8", null ],
    [ "KEY_LEVEL", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a2120d471066e0f05e728929c3bf16453.html#a2120d471066e0f05e728929c3bf16453", null ],
    [ "KEY_PID", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_aa793c1946f22201052adc6e841fc4fd2.html#aa793c1946f22201052adc6e841fc4fd2", null ],
    [ "PREFIX", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_a54c03aae6eb730123a7f06c44fc6cb3a.html#a54c03aae6eb730123a7f06c44fc6cb3a", null ],
    [ "Instance", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension_ae901ad90c5109c4b51a4843212a7cbdd.html#ae901ad90c5109c4b51a4843212a7cbdd", null ]
];