var NAVTREE =
[
  [ "OpenXes.Net", "index.html", [
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a4b9d33f664f5fa1f2b69b6936bb16b06.html#a4b9d33f664f5fa1f2b69b6936bb16b06",
"classOpenXesNet_1_1info_1_1XAttributeInfo_aabfe06b52df43bcd7b8d707a71a2bd4d.html#aabfe06b52df43bcd7b8d707a71a2bd4d",
"classOpenXesNet_1_1model_1_1XAttributeTimestamp_a450887f6453450869f3cff0af16681ba.html#a450887f6453450869f3cff0af16681ba",
"interfaceOpenXesNet_1_1model_1_1IXAttributeCollection_a05953b22a5fe598af75dc3c8c0352863.html#a05953b22a5fe598af75dc3c8c0352863"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';