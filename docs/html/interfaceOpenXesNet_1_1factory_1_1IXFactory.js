var interfaceOpenXesNet_1_1factory_1_1IXFactory =
[
    [ "CreateAttributeBoolean", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a048c7df4943e1a32f2fedfa91d283da4.html#a048c7df4943e1a32f2fedfa91d283da4", null ],
    [ "CreateAttributeContainer", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a4cf1f5cb26a66f322a98c68addb828f7.html#a4cf1f5cb26a66f322a98c68addb828f7", null ],
    [ "CreateAttributeContinuous", "interfaceOpenXesNet_1_1factory_1_1IXFactory_afe6e3e937b0366e28e12924625279458.html#afe6e3e937b0366e28e12924625279458", null ],
    [ "CreateAttributeDiscrete", "interfaceOpenXesNet_1_1factory_1_1IXFactory_adabca475435abd0607f7837e20273ee4.html#adabca475435abd0607f7837e20273ee4", null ],
    [ "CreateAttributeID", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a8e51f01376ff6f4e11ebcf11c6e80d13.html#a8e51f01376ff6f4e11ebcf11c6e80d13", null ],
    [ "CreateAttributeList", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a17257429c63106750221407f13dd2a72.html#a17257429c63106750221407f13dd2a72", null ],
    [ "CreateAttributeLiteral", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a3a33a326c2796fb8ce87e7faf2eae629.html#a3a33a326c2796fb8ce87e7faf2eae629", null ],
    [ "CreateAttributeMap", "interfaceOpenXesNet_1_1factory_1_1IXFactory_aa0d11c90295d6e47c28af3d524ce92f1.html#aa0d11c90295d6e47c28af3d524ce92f1", null ],
    [ "CreateAttributeTimestamp", "interfaceOpenXesNet_1_1factory_1_1IXFactory_ad7302007834c98996a79bdc3943bc102.html#ad7302007834c98996a79bdc3943bc102", null ],
    [ "CreateAttributeTimestamp", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a54293e315e24d1a70ae54dbfad99e36d.html#a54293e315e24d1a70ae54dbfad99e36d", null ],
    [ "CreateEvent", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a008de8b0857ede3c8fdbca7cf4e1b3d2.html#a008de8b0857ede3c8fdbca7cf4e1b3d2", null ],
    [ "CreateEvent", "interfaceOpenXesNet_1_1factory_1_1IXFactory_ae514ada76ef1ef5bddd0cd17099ea6ee.html#ae514ada76ef1ef5bddd0cd17099ea6ee", null ],
    [ "CreateEvent", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a664909863ddee4f4adcc4233dbb05d91.html#a664909863ddee4f4adcc4233dbb05d91", null ],
    [ "CreateLog", "interfaceOpenXesNet_1_1factory_1_1IXFactory_abdfd9033a078a2d0a7973864cfcf252c.html#abdfd9033a078a2d0a7973864cfcf252c", null ],
    [ "CreateLog", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a65d8a07d38405d4e78f869a95a56370e.html#a65d8a07d38405d4e78f869a95a56370e", null ],
    [ "CreateTrace", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a804967b86c4dfb81ee362037ae8e5f4b.html#a804967b86c4dfb81ee362037ae8e5f4b", null ],
    [ "CreateTrace", "interfaceOpenXesNet_1_1factory_1_1IXFactory_aaa21c3caf355808d4f7d20acd64ebd0d.html#aaa21c3caf355808d4f7d20acd64ebd0d", null ],
    [ "Author", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a30e0fa17fd076798f512ec94d69a8e2e.html#a30e0fa17fd076798f512ec94d69a8e2e", null ],
    [ "Description", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a17b400fe2f8fb4038c10993c030ee762.html#a17b400fe2f8fb4038c10993c030ee762", null ],
    [ "Name", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a7468741208faf62a47173322cbb1fbec.html#a7468741208faf62a47173322cbb1fbec", null ],
    [ "Uri", "interfaceOpenXesNet_1_1factory_1_1IXFactory_aba28e09662155be14ad995674305887f.html#aba28e09662155be14ad995674305887f", null ],
    [ "Vendor", "interfaceOpenXesNet_1_1factory_1_1IXFactory_a184a8d13657509c0f1152bcdabd8f347.html#a184a8d13657509c0f1152bcdabd8f347", null ]
];