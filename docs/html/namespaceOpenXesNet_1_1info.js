var namespaceOpenXesNet_1_1info =
[
    [ "IXAttributeInfo", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo.html", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo" ],
    [ "IXAttributeNameMap", "interfaceOpenXesNet_1_1info_1_1IXAttributeNameMap.html", "interfaceOpenXesNet_1_1info_1_1IXAttributeNameMap" ],
    [ "IXLogInfo", "interfaceOpenXesNet_1_1info_1_1IXLogInfo.html", "interfaceOpenXesNet_1_1info_1_1IXLogInfo" ],
    [ "IXTimeBounds", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds.html", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds" ],
    [ "XAttributeInfo", "classOpenXesNet_1_1info_1_1XAttributeInfo.html", "classOpenXesNet_1_1info_1_1XAttributeInfo" ],
    [ "XAttributeNameMap", "classOpenXesNet_1_1info_1_1XAttributeNameMap.html", "classOpenXesNet_1_1info_1_1XAttributeNameMap" ],
    [ "XGlobalAttributeNameMap", "classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap.html", "classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap" ],
    [ "XLogInfo", "classOpenXesNet_1_1info_1_1XLogInfo.html", "classOpenXesNet_1_1info_1_1XLogInfo" ],
    [ "XLogInfoFactory", "classOpenXesNet_1_1info_1_1XLogInfoFactory.html", "classOpenXesNet_1_1info_1_1XLogInfoFactory" ],
    [ "XTimeBounds", "classOpenXesNet_1_1info_1_1XTimeBounds.html", "classOpenXesNet_1_1info_1_1XTimeBounds" ]
];