var classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension =
[
    [ "AssignModelReferences", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_a2dc219a103beda5277dd8731482ef193.html#a2dc219a103beda5277dd8731482ef193", null ],
    [ "AssignModelReferenceUris", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_a20a06d86bb15a668d43bed64b790b2ed.html#a20a06d86bb15a668d43bed64b790b2ed", null ],
    [ "ExtractModelReferences", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_a18380154ae074ceaca3da2501908d78f.html#a18380154ae074ceaca3da2501908d78f", null ],
    [ "ExtractModelReferenceURIs", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_a485cbc98bcc8856a580f838d15657cc6.html#a485cbc98bcc8856a580f838d15657cc6", null ],
    [ "ATTR_MODELREFERENCE", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_af8aefb975fb91d434bd455ce4492a76c.html#af8aefb975fb91d434bd455ce4492a76c", null ],
    [ "EXTENSION_URI", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_ace8aea38dc8d9cf426d9ed414a1e088f.html#ace8aea38dc8d9cf426d9ed414a1e088f", null ],
    [ "KEY_MODELREFERENCE", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_a7ce3933461453bee3118b556a8e0b6cd.html#a7ce3933461453bee3118b556a8e0b6cd", null ],
    [ "Instance", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension_a89e100d04afeb530293aef900e83ef14.html#a89e100d04afeb530293aef900e83ef14", null ]
];