var interfaceOpenXesNet_1_1info_1_1IXAttributeInfo =
[
    [ "GetAttributeKeys", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_a2a9e08946ade0da15bb87d2cf373466c.html#a2a9e08946ade0da15bb87d2cf373466c", null ],
    [ "GetAttributes", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_ad63f312495d5fb5b651568395c95ee9e.html#ad63f312495d5fb5b651568395c95ee9e", null ],
    [ "GetAttributesForExtension", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_ab384530556a6d070f482a10326318558.html#ab384530556a6d070f482a10326318558", null ],
    [ "GetAttributesForType", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_aacd8daa8d9acc477f7aa8222ab389503.html#aacd8daa8d9acc477f7aa8222ab389503", null ],
    [ "GetAttributesWithoutExtension", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_a53b93c8cdc7a1dd4fc70100ab51012f3.html#a53b93c8cdc7a1dd4fc70100ab51012f3", null ],
    [ "GetFrequency", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_a63b1a001171f10711ce0f8a567b4cac3.html#a63b1a001171f10711ce0f8a567b4cac3", null ],
    [ "GetFrequency", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_a3e76b0bfda797318fc1b02d5f810fd32.html#a3e76b0bfda797318fc1b02d5f810fd32", null ],
    [ "GetKeysForExtension", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_a78593dc0259b61b7ca20f8f0b2957e3d.html#a78593dc0259b61b7ca20f8f0b2957e3d", null ],
    [ "GetKeysForType", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_a05097c3070c45a1e910bd287b9edd9e7.html#a05097c3070c45a1e910bd287b9edd9e7", null ],
    [ "GetKeysWithoutExtension", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_a82fcbd036cf61c88559bc3855f9b51ca.html#a82fcbd036cf61c88559bc3855f9b51ca", null ],
    [ "GetRelativeFrequency", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_ae2126a10b00ba83dafad20de696009fa.html#ae2126a10b00ba83dafad20de696009fa", null ],
    [ "GetRelativeFrequency", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo_aa5553bbc0fd695d62146a71eace43593.html#aa5553bbc0fd695d62146a71eace43593", null ]
];