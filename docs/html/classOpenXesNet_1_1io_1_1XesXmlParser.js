var classOpenXesNet_1_1io_1_1XesXmlParser =
[
    [ "XesXmlParser", "classOpenXesNet_1_1io_1_1XesXmlParser_ab4ee9a7e0d59fde423a7e425c5ae7694.html#ab4ee9a7e0d59fde423a7e425c5ae7694", null ],
    [ "XesXmlParser", "classOpenXesNet_1_1io_1_1XesXmlParser_a1cfe1601b6107a59ecfef3df553f87ad.html#a1cfe1601b6107a59ecfef3df553f87ad", null ],
    [ "CanParse", "classOpenXesNet_1_1io_1_1XesXmlParser_a5db4243412359d18229a1e075a305f69.html#a5db4243412359d18229a1e075a305f69", null ],
    [ "Dispose", "classOpenXesNet_1_1io_1_1XesXmlParser_a124780ea0314b1b398774f24273db37f.html#a124780ea0314b1b398774f24273db37f", null ],
    [ "Parse", "classOpenXesNet_1_1io_1_1XesXmlParser_a0e982e3511614f5f936fa1d33ff91b27.html#a0e982e3511614f5f936fa1d33ff91b27", null ],
    [ "Parse", "classOpenXesNet_1_1io_1_1XesXmlParser_a00f8abfa56ad43a2965839d3b2b00b86.html#a00f8abfa56ad43a2965839d3b2b00b86", null ],
    [ "factory", "classOpenXesNet_1_1io_1_1XesXmlParser_a25d4a6047d4dc54f00d8760c164b09ac.html#a25d4a6047d4dc54f00d8760c164b09ac", null ],
    [ "XES_URI", "classOpenXesNet_1_1io_1_1XesXmlParser_a95517e0d8156992d425cd0e99903ecb4.html#a95517e0d8156992d425cd0e99903ecb4", null ],
    [ "Author", "classOpenXesNet_1_1io_1_1XesXmlParser_a16cdc3dd43aade01d75070345271c3ca.html#a16cdc3dd43aade01d75070345271c3ca", null ],
    [ "Description", "classOpenXesNet_1_1io_1_1XesXmlParser_aba6210e47126834d7343377a65c0bd60.html#aba6210e47126834d7343377a65c0bd60", null ],
    [ "Name", "classOpenXesNet_1_1io_1_1XesXmlParser_ad06d78c1ab2434ed06e514c294a1d74a.html#ad06d78c1ab2434ed06e514c294a1d74a", null ]
];