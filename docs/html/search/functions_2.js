var searchData=
[
  ['debug',['Debug',['../classOpenXesNet_1_1logging_1_1XLogging_a9f6b6525562aac063c3342a0b81d20dd.html#a9f6b6525562aac063c3342a0b81d20dd',1,'OpenXesNet::logging::XLogging']]],
  ['decode',['Decode',['../classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_aa54bd1043f28a4c76fd9da2c7e562a8d.html#aa54bd1043f28a4c76fd9da2c7e562a8d',1,'OpenXesNet::extension::std::XLifecycleExtension']]],
  ['deriveeventclasses',['DeriveEventClasses',['../classOpenXesNet_1_1classification_1_1XEventClasses_a95dc6c3ad33825334018ed0a73533b68.html#a95dc6c3ad33825334018ed0a73533b68',1,'OpenXesNet::classification::XEventClasses']]],
  ['deriveprototype',['DerivePrototype',['../classOpenXesNet_1_1util_1_1XAttributeUtils_a31e6c7e7ad029e18e50f2a4eb93fe500.html#a31e6c7e7ad029e18e50f2a4eb93fe500',1,'OpenXesNet::util::XAttributeUtils']]],
  ['determineos',['DetermineOS',['../classOpenXesNet_1_1util_1_1XRuntimeUtils_a058f71d2e6ff254f4c2fe4ad2dd018c2.html#a058f71d2e6ff254f4c2fe4ad2dd018c2',1,'OpenXesNet::util::XRuntimeUtils']]],
  ['dispose',['Dispose',['../classOpenXesNet_1_1io_1_1XesXmlParser_a124780ea0314b1b398774f24273db37f.html#a124780ea0314b1b398774f24273db37f',1,'OpenXesNet.io.XesXmlParser.Dispose()'],['../classOpenXesNet_1_1io_1_1XParser_a18286fedbc20100dff8a086469ecc4a8.html#a18286fedbc20100dff8a086469ecc4a8',1,'OpenXesNet.io.XParser.Dispose()']]]
];
