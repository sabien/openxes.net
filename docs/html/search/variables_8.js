var searchData=
[
  ['mapping_5fdutch',['MAPPING_DUTCH',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_af7fd9107d9956a715b66903d72018f18.html#af7fd9107d9956a715b66903d72018f18',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['mapping_5fenglish',['MAPPING_ENGLISH',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_a109525ee289f182a723452b87e54508c.html#a109525ee289f182a723452b87e54508c',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['mapping_5ffrench',['MAPPING_FRENCH',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_ae19df38d4188d052789ebb130f1491d5.html#ae19df38d4188d052789ebb130f1491d5',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['mapping_5fgerman',['MAPPING_GERMAN',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_ab165333e7bffa30cdf35bbf29c7c042b.html#ab165333e7bffa30cdf35bbf29c7c042b',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['mapping_5fitalian',['MAPPING_ITALIAN',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_aa369e1356289c8843972751288b9bc8d.html#aa369e1356289c8843972751288b9bc8d',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['mapping_5fportuguese',['MAPPING_PORTUGUESE',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_a1a499a5efd0d1e6a04c21f72e1931d11.html#a1a499a5efd0d1e6a04c21f72e1931d11',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['mapping_5fspanish',['MAPPING_SPANISH',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_ad2d062536a139b405e5dea8b3fc638ac.html#ad2d062536a139b405e5dea8b3fc638ac',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['mapping_5fstandard',['MAPPING_STANDARD',['../classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap_afc700ddf2290c94c9b97fe93b0347e30.html#afc700ddf2290c94c9b97fe93b0347e30',1,'OpenXesNet::info::XGlobalAttributeNameMap']]],
  ['metaattributeinfo',['metaAttributeInfo',['../classOpenXesNet_1_1info_1_1XLogInfo_accc0865458c5ec6e3da81b9e2cd10ced.html#accc0865458c5ec6e3da81b9e2cd10ced',1,'OpenXesNet::info::XLogInfo']]],
  ['metaattributes',['metaAttributes',['../classOpenXesNet_1_1extension_1_1XExtension_a2e35b6781489308e42bdce51146ef183.html#a2e35b6781489308e42bdce51146ef183',1,'OpenXesNet::extension::XExtension']]]
];
