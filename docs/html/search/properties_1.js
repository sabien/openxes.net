var searchData=
[
  ['classes',['Classes',['../classOpenXesNet_1_1classification_1_1XEventClasses_ac0db0424c84111d1d507879b08ab06d1.html#ac0db0424c84111d1d507879b08ab06d1',1,'OpenXesNet::classification::XEventClasses']]],
  ['classifier',['Classifier',['../classOpenXesNet_1_1classification_1_1XEventClasses_a2c70ba2a969499df375abd00a0adee2e.html#a2c70ba2a969499df375abd00a0adee2e',1,'OpenXesNet::classification::XEventClasses']]],
  ['classifiers',['Classifiers',['../interfaceOpenXesNet_1_1model_1_1IXLog_af859c253fda7db1c7b2c3c4d22a77401.html#af859c253fda7db1c7b2c3c4d22a77401',1,'OpenXesNet.model.IXLog.Classifiers()'],['../classOpenXesNet_1_1model_1_1XLog_abc25f189cc1b681a5aaeaa31204cb1dc.html#abc25f189cc1b681a5aaeaa31204cb1dc',1,'OpenXesNet.model.XLog.Classifiers()']]],
  ['count',['Count',['../classOpenXesNet_1_1classification_1_1XEventClasses_a6fa39cff38f71aad10352c2c493fa7f0.html#a6fa39cff38f71aad10352c2c493fa7f0',1,'OpenXesNet.classification.XEventClasses.Count()'],['../classOpenXesNet_1_1model_1_1XAttributeMapLazy_a07f8f4722ce4ca5e7ecbb750b3c1c67a.html#a07f8f4722ce4ca5e7ecbb750b3c1c67a',1,'OpenXesNet.model.XAttributeMapLazy.Count()']]],
  ['currentdefault',['CurrentDefault',['../classOpenXesNet_1_1util_1_1XRegistry_af1860a44d455f13befc4f21e4705c6ad.html#af1860a44d455f13befc4f21e4705c6ad',1,'OpenXesNet::util::XRegistry']]]
];
