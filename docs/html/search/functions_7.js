var searchData=
[
  ['incrementsize',['IncrementSize',['../classOpenXesNet_1_1classification_1_1XEventClass_a37ca29fcff28b30ab86638fed6b2fa30.html#a37ca29fcff28b30ab86638fed6b2fa30',1,'OpenXesNet::classification::XEventClass']]],
  ['info',['Info',['../classOpenXesNet_1_1logging_1_1XLogging_a18a999ce4613e56810680bcbe3061927.html#a18a999ce4613e56810680bcbe3061927',1,'OpenXesNet::logging::XLogging']]],
  ['init',['Init',['../classOpenXesNet_1_1model_1_1XVisitor_aa28374c3e4dcc1114b497d26744c84ff.html#aa28374c3e4dcc1114b497d26744c84ff',1,'OpenXesNet::model::XVisitor']]],
  ['insertordered',['InsertOrdered',['../interfaceOpenXesNet_1_1model_1_1IXTrace_a141b4f4768f9cd0203b053b20bb052b6.html#a141b4f4768f9cd0203b053b20bb052b6',1,'OpenXesNet.model.IXTrace.InsertOrdered()'],['../classOpenXesNet_1_1model_1_1XTrace_ac2fb034b48091c7726ec9f891752b90a.html#ac2fb034b48091c7726ec9f891752b90a',1,'OpenXesNet.model.XTrace.InsertOrdered()']]],
  ['iscontained',['IsContained',['../classOpenXesNet_1_1util_1_1XRegistry_a62ec341b33704656b16ba7941f434a53.html#a62ec341b33704656b16ba7941f434a53',1,'OpenXesNet::util::XRegistry']]],
  ['isempty',['IsEmpty',['../classOpenXesNet_1_1model_1_1XAttributeMapLazy_a21c857c00830635b2bea111264bc2e94.html#a21c857c00830635b2bea111264bc2e94',1,'OpenXesNet::model::XAttributeMapLazy']]],
  ['isrunninglinux',['IsRunningLinux',['../classOpenXesNet_1_1util_1_1XRuntimeUtils_aa821851d269b91b1f00ed98467ee9ac1.html#aa821851d269b91b1f00ed98467ee9ac1',1,'OpenXesNet::util::XRuntimeUtils']]],
  ['isrunningmacosx',['IsRunningMacOsX',['../classOpenXesNet_1_1util_1_1XRuntimeUtils_a12bb67aa9ae5fc1e2e500871805eaa64.html#a12bb67aa9ae5fc1e2e500871805eaa64',1,'OpenXesNet::util::XRuntimeUtils']]],
  ['isrunningunix',['IsRunningUnix',['../classOpenXesNet_1_1util_1_1XRuntimeUtils_ac2a55f4f082d38ef4dd04097bf562f3f.html#ac2a55f4f082d38ef4dd04097bf562f3f',1,'OpenXesNet::util::XRuntimeUtils']]],
  ['isrunningwindows',['IsRunningWindows',['../classOpenXesNet_1_1util_1_1XRuntimeUtils_ae2bee5d99d8b6f05e2d3f1ece1597b2b.html#ae2bee5d99d8b6f05e2d3f1ece1597b2b',1,'OpenXesNet::util::XRuntimeUtils']]],
  ['iswithin',['IsWithin',['../interfaceOpenXesNet_1_1info_1_1IXTimeBounds_a2a0fab4cad7c7a5ef6605bc560cc7d55.html#a2a0fab4cad7c7a5ef6605bc560cc7d55',1,'OpenXesNet.info.IXTimeBounds.IsWithin()'],['../classOpenXesNet_1_1info_1_1XTimeBounds_a548c9bbbf6e7f4fb9d5a8d4764b06175.html#a548c9bbbf6e7f4fb9d5a8d4764b06175',1,'OpenXesNet.info.XTimeBounds.IsWithin()']]]
];
