var classOpenXesNet_1_1model_1_1XAttributeTimestamp =
[
    [ "XAttributeTimestamp", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_af82d692df712d621e6a29d60ba627b29.html#af82d692df712d621e6a29d60ba627b29", null ],
    [ "XAttributeTimestamp", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_acb40a2373739d124e02a57805185c8b1.html#acb40a2373739d124e02a57805185c8b1", null ],
    [ "XAttributeTimestamp", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a9742cd3bb3774e3d6e1937c8b0e0b031.html#a9742cd3bb3774e3d6e1937c8b0e0b031", null ],
    [ "XAttributeTimestamp", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a6a1558c17189da091412052f4daae7fb.html#a6a1558c17189da091412052f4daae7fb", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a58b39637be8d697b2fcde71ea2e114f5.html#a58b39637be8d697b2fcde71ea2e114f5", null ],
    [ "CompareTo", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a450887f6453450869f3cff0af16681ba.html#a450887f6453450869f3cff0af16681ba", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a2d8729f323e35aeb2a18aa703309261d.html#a2d8729f323e35aeb2a18aa703309261d", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a8481f53660292459d67a799f49d166ea.html#a8481f53660292459d67a799f49d166ea", null ],
    [ "Parse", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_acead56686a792a6e3083964ef887e671.html#acead56686a792a6e3083964ef887e671", null ],
    [ "ToString", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a0c1cec911711172e8c938f1b1bfd41a1.html#a0c1cec911711172e8c938f1b1bfd41a1", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_ac252eac03e85392402d2fdf4d96fe896.html#ac252eac03e85392402d2fdf4d96fe896", null ],
    [ "ValueMillis", "classOpenXesNet_1_1model_1_1XAttributeTimestamp_a10273f74833fdf0aad074eba28bdbb0d.html#a10273f74833fdf0aad074eba28bdbb0d", null ]
];