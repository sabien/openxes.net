var classOpenXesNet_1_1util_1_1XAttributeUtils =
[
    [ "ComposeAttribute", "classOpenXesNet_1_1util_1_1XAttributeUtils_a1912ca8ad145094c22195f066889b7cc.html#a1912ca8ad145094c22195f066889b7cc", null ],
    [ "DerivePrototype", "classOpenXesNet_1_1util_1_1XAttributeUtils_a31e6c7e7ad029e18e50f2a4eb93fe500.html#a31e6c7e7ad029e18e50f2a4eb93fe500", null ],
    [ "ExtractExtensions", "classOpenXesNet_1_1util_1_1XAttributeUtils_a8c8b60ea1cc95198aa86d9304079729a.html#a8c8b60ea1cc95198aa86d9304079729a", null ],
    [ "GetType", "classOpenXesNet_1_1util_1_1XAttributeUtils_a22c180c92709bedd27b38614e0cf116a.html#a22c180c92709bedd27b38614e0cf116a", null ],
    [ "GetTypeString", "classOpenXesNet_1_1util_1_1XAttributeUtils_a9c1529e0f2cfca9e95e8a1753fd2643e.html#a9c1529e0f2cfca9e95e8a1753fd2643e", null ]
];