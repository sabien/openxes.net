var interfaceOpenXesNet_1_1model_1_1IXAttributable =
[
    [ "GetAttributes", "interfaceOpenXesNet_1_1model_1_1IXAttributable_a370ecf002ceeeace1207771670748dfd.html#a370ecf002ceeeace1207771670748dfd", null ],
    [ "HasAttributes", "interfaceOpenXesNet_1_1model_1_1IXAttributable_a1d5df2d1d46fc655e7e815e73368d8c3.html#a1d5df2d1d46fc655e7e815e73368d8c3", null ],
    [ "SetAttributes", "interfaceOpenXesNet_1_1model_1_1IXAttributable_a9524528ed356c8f36de3aaddccece316.html#a9524528ed356c8f36de3aaddccece316", null ],
    [ "Extensions", "interfaceOpenXesNet_1_1model_1_1IXAttributable_a4855f166f53af8a81d07129a500ac609.html#a4855f166f53af8a81d07129a500ac609", null ]
];