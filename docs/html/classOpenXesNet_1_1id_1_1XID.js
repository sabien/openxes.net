var classOpenXesNet_1_1id_1_1XID =
[
    [ "XID", "classOpenXesNet_1_1id_1_1XID_a1983b4bcc84536cb002c03add839db33.html#a1983b4bcc84536cb002c03add839db33", null ],
    [ "XID", "classOpenXesNet_1_1id_1_1XID_a6f5c2db07e4032033cb61f8dcd880e20.html#a6f5c2db07e4032033cb61f8dcd880e20", null ],
    [ "Clone", "classOpenXesNet_1_1id_1_1XID_a315380c6723bd26e662769dc245a8d0e.html#a315380c6723bd26e662769dc245a8d0e", null ],
    [ "CompareTo", "classOpenXesNet_1_1id_1_1XID_a65f91cb1db36a60a93df2acdafb1d2c9.html#a65f91cb1db36a60a93df2acdafb1d2c9", null ],
    [ "Equals", "classOpenXesNet_1_1id_1_1XID_a9cf3dfff52c48e3e925c1bde1318a674.html#a9cf3dfff52c48e3e925c1bde1318a674", null ],
    [ "GetHashCode", "classOpenXesNet_1_1id_1_1XID_a5a271e5fe2ad16755cfea04b0892ca78.html#a5a271e5fe2ad16755cfea04b0892ca78", null ],
    [ "Parse", "classOpenXesNet_1_1id_1_1XID_ac885983d921b06f3f86cfb6bda91bb48.html#ac885983d921b06f3f86cfb6bda91bb48", null ],
    [ "ToString", "classOpenXesNet_1_1id_1_1XID_a470fd7ec6861b2bf657278053cd139dd.html#a470fd7ec6861b2bf657278053cd139dd", null ]
];