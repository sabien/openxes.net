var classOpenXesNet_1_1model_1_1XAttributeDiscrete =
[
    [ "XAttributeDiscrete", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_ae31773b20706dcc1f812deb74c648f60.html#ae31773b20706dcc1f812deb74c648f60", null ],
    [ "XAttributeDiscrete", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_ac0e4322d1450c6f0d3f0d4bfa81bc1ef.html#ac0e4322d1450c6f0d3f0d4bfa81bc1ef", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_a66307e6a2c7c2584600165d5e1909ae0.html#a66307e6a2c7c2584600165d5e1909ae0", null ],
    [ "CompareTo", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_a4f95b81753d9bcb20a433421d5b52bd6.html#a4f95b81753d9bcb20a433421d5b52bd6", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_a5ccacaeccba6458038c5ae6ac1deb824.html#a5ccacaeccba6458038c5ae6ac1deb824", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_ab35066aaf3fdf77735916a7e2de673c4.html#ab35066aaf3fdf77735916a7e2de673c4", null ],
    [ "ToString", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_a70ea22d344e5c1d0f04bbb6a20d47b79.html#a70ea22d344e5c1d0f04bbb6a20d47b79", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttributeDiscrete_a9208da4867c6c62006f13a088f973d5a.html#a9208da4867c6c62006f13a088f973d5a", null ]
];