var interfaceOpenXesNet_1_1info_1_1IXTimeBounds =
[
    [ "GetEndDate", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds_a934982c0b632aa7a49d1066dce9f0b2b.html#a934982c0b632aa7a49d1066dce9f0b2b", null ],
    [ "GetStartDate", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds_a8627f33346d6d47c0d9fcefc10ba0724.html#a8627f33346d6d47c0d9fcefc10ba0724", null ],
    [ "IsWithin", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds_a2a0fab4cad7c7a5ef6605bc560cc7d55.html#a2a0fab4cad7c7a5ef6605bc560cc7d55", null ],
    [ "Register", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds_a8975fd26affef563ed277129a5e1ac95.html#a8975fd26affef563ed277129a5e1ac95", null ],
    [ "Register", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds_a6f97bf3e21cc57ae350c87c0c0c4a578.html#a6f97bf3e21cc57ae350c87c0c0c4a578", null ],
    [ "Register", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds_aff169d10ef23c667e4abe36640daa0bc.html#aff169d10ef23c667e4abe36640daa0bc", null ],
    [ "ToString", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds_ab5444f04f4c8aeed8d449ff3fb5a177a.html#ab5444f04f4c8aeed8d449ff3fb5a177a", null ]
];