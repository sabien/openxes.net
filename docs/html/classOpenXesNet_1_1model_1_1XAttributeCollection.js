var classOpenXesNet_1_1model_1_1XAttributeCollection =
[
    [ "XAttributeCollection", "classOpenXesNet_1_1model_1_1XAttributeCollection_a9adea399ab41e00e2c23d2c5c59c70e3.html#a9adea399ab41e00e2c23d2c5c59c70e3", null ],
    [ "XAttributeCollection", "classOpenXesNet_1_1model_1_1XAttributeCollection_a1dc60ae75272c56b31ef97c544343437.html#a1dc60ae75272c56b31ef97c544343437", null ],
    [ "AddToCollection", "classOpenXesNet_1_1model_1_1XAttributeCollection_ac281f9740708904ef106d14311fd4fc2.html#ac281f9740708904ef106d14311fd4fc2", null ],
    [ "GetCollection", "classOpenXesNet_1_1model_1_1XAttributeCollection_a8f4ea7ffa87c122b3e5ff0b8133127c7.html#a8f4ea7ffa87c122b3e5ff0b8133127c7", null ],
    [ "RemoveFromCollection", "classOpenXesNet_1_1model_1_1XAttributeCollection_a09bca5822b65eb76bed2f46169e38376.html#a09bca5822b65eb76bed2f46169e38376", null ],
    [ "ToString", "classOpenXesNet_1_1model_1_1XAttributeCollection_a374882613bb15e962e868b7522b990c5.html#a374882613bb15e962e868b7522b990c5", null ],
    [ "collection", "classOpenXesNet_1_1model_1_1XAttributeCollection_a9190b78beaabcad9bd556e41bf7dc182.html#a9190b78beaabcad9bd556e41bf7dc182", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttributeCollection_a1842741b6d228b10e7e31b60c172f471.html#a1842741b6d228b10e7e31b60c172f471", null ]
];