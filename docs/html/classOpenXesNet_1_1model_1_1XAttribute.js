var classOpenXesNet_1_1model_1_1XAttribute =
[
    [ "XAttribute", "classOpenXesNet_1_1model_1_1XAttribute_a3b6b2e554d210bb9c6d216be26974e4e.html#a3b6b2e554d210bb9c6d216be26974e4e", null ],
    [ "XAttribute", "classOpenXesNet_1_1model_1_1XAttribute_aebcadcea72dc6de7f29e368f83af807c.html#aebcadcea72dc6de7f29e368f83af807c", null ],
    [ "XAttribute", "classOpenXesNet_1_1model_1_1XAttribute_a3b6b2e554d210bb9c6d216be26974e4e.html#a3b6b2e554d210bb9c6d216be26974e4e", null ],
    [ "XAttribute", "classOpenXesNet_1_1model_1_1XAttribute_aebcadcea72dc6de7f29e368f83af807c.html#aebcadcea72dc6de7f29e368f83af807c", null ],
    [ "Accept", "classOpenXesNet_1_1model_1_1XAttribute_a5c70d0e6b00cf803b5be7c32eeb56494.html#a5c70d0e6b00cf803b5be7c32eeb56494", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XAttribute_a8d549b49bc65595a87987aab0aafe2cc.html#a8d549b49bc65595a87987aab0aafe2cc", null ],
    [ "CompareTo", "classOpenXesNet_1_1model_1_1XAttribute_a88aac1cc8f0c010788dc442ffd699798.html#a88aac1cc8f0c010788dc442ffd699798", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XAttribute_a7e38d36559eb82ff344cf755d50897bf.html#a7e38d36559eb82ff344cf755d50897bf", null ],
    [ "GetAttributes", "classOpenXesNet_1_1model_1_1XAttribute_a0c20ea94eb1cbf83acc305b3850caa07.html#a0c20ea94eb1cbf83acc305b3850caa07", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XAttribute_a5f09ac0969e3d7aab8bbaf7e2959967b.html#a5f09ac0969e3d7aab8bbaf7e2959967b", null ],
    [ "HasAttributes", "classOpenXesNet_1_1model_1_1XAttribute_a5086800e2fdf33fe1ead0fb955fbd1b0.html#a5086800e2fdf33fe1ead0fb955fbd1b0", null ],
    [ "SetAttributes", "classOpenXesNet_1_1model_1_1XAttribute_ae71ed6587fd8aa4840f25b9d2b53aabf.html#ae71ed6587fd8aa4840f25b9d2b53aabf", null ],
    [ "Extension", "classOpenXesNet_1_1model_1_1XAttribute_afcd34712beb1f18ef627d30695145277.html#afcd34712beb1f18ef627d30695145277", null ],
    [ "Extensions", "classOpenXesNet_1_1model_1_1XAttribute_afcf8450396e48e9ce3c1e6f026a31d46.html#afcf8450396e48e9ce3c1e6f026a31d46", null ],
    [ "Key", "classOpenXesNet_1_1model_1_1XAttribute_a9ad6464a42f4b630941b70f5ac0fa4f8.html#a9ad6464a42f4b630941b70f5ac0fa4f8", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttribute_ae830b56df35ed552c3a97cc65ad92f5f.html#ae830b56df35ed552c3a97cc65ad92f5f", null ]
];