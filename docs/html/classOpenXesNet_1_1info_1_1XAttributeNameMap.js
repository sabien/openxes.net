var classOpenXesNet_1_1info_1_1XAttributeNameMap =
[
    [ "XAttributeNameMap", "classOpenXesNet_1_1info_1_1XAttributeNameMap_a259618ff5651cb0ccbb9085ef8456db8.html#a259618ff5651cb0ccbb9085ef8456db8", null ],
    [ "Get", "classOpenXesNet_1_1info_1_1XAttributeNameMap_aa7bff1007fd658fee907ebfc4553900e.html#aa7bff1007fd658fee907ebfc4553900e", null ],
    [ "Get", "classOpenXesNet_1_1info_1_1XAttributeNameMap_a176ec1cf5ec115d2c557940396e918b5.html#a176ec1cf5ec115d2c557940396e918b5", null ],
    [ "RegisterMapping", "classOpenXesNet_1_1info_1_1XAttributeNameMap_a31f02611f81f129879b338250be83068.html#a31f02611f81f129879b338250be83068", null ],
    [ "RegisterMapping", "classOpenXesNet_1_1info_1_1XAttributeNameMap_a210b6c10608803ca94fe06e118666eba.html#a210b6c10608803ca94fe06e118666eba", null ],
    [ "ToString", "classOpenXesNet_1_1info_1_1XAttributeNameMap_a8f41a77f317c44b6ae8c167e118c0090.html#a8f41a77f317c44b6ae8c167e118c0090", null ],
    [ "MappingName", "classOpenXesNet_1_1info_1_1XAttributeNameMap_af9c74f42c7d8cc044f6a2745d7b09472.html#af9c74f42c7d8cc044f6a2745d7b09472", null ]
];