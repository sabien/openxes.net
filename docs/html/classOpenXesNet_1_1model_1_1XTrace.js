var classOpenXesNet_1_1model_1_1XTrace =
[
    [ "XTrace", "classOpenXesNet_1_1model_1_1XTrace_af57c479dab233f176c3b5d94d991728b.html#af57c479dab233f176c3b5d94d991728b", null ],
    [ "Accept", "classOpenXesNet_1_1model_1_1XTrace_a3e2977e44a841ffd0f27b8c3ab4325e4.html#a3e2977e44a841ffd0f27b8c3ab4325e4", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XTrace_af89f6cba97b7fb282297324994498613.html#af89f6cba97b7fb282297324994498613", null ],
    [ "GetAttributes", "classOpenXesNet_1_1model_1_1XTrace_a1e444037bdd54ed771d196563bef0aae.html#a1e444037bdd54ed771d196563bef0aae", null ],
    [ "HasAttributes", "classOpenXesNet_1_1model_1_1XTrace_ab84a7bd83f659cde10d2a62ecaea2b18.html#ab84a7bd83f659cde10d2a62ecaea2b18", null ],
    [ "InsertOrdered", "classOpenXesNet_1_1model_1_1XTrace_ac2fb034b48091c7726ec9f891752b90a.html#ac2fb034b48091c7726ec9f891752b90a", null ],
    [ "SetAttributes", "classOpenXesNet_1_1model_1_1XTrace_addb6ac4b3a09ceb494e7e78a10899e5e.html#addb6ac4b3a09ceb494e7e78a10899e5e", null ],
    [ "Extensions", "classOpenXesNet_1_1model_1_1XTrace_a71ac22e760903d434042146afccc1567.html#a71ac22e760903d434042146afccc1567", null ]
];