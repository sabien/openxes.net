var classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension =
[
    [ "assignGroup", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a3c744314ab2f08d3bd22cb0b7c078542.html#a3c744314ab2f08d3bd22cb0b7c078542", null ],
    [ "assignResource", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a610e7b8eb3f27dbb7b255f93ea8b9c56.html#a610e7b8eb3f27dbb7b255f93ea8b9c56", null ],
    [ "assignRole", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_ab73f13d3887ea810f74513544a8f4acb.html#ab73f13d3887ea810f74513544a8f4acb", null ],
    [ "extractGroup", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_aec35f613c31442f7dab21dc899d852be.html#aec35f613c31442f7dab21dc899d852be", null ],
    [ "extractResource", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a91d1f1bd3c7fe8b6ff10c2ef41629191.html#a91d1f1bd3c7fe8b6ff10c2ef41629191", null ],
    [ "extractRole", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a535d48664c28df6b87f351faeb7be30b.html#a535d48664c28df6b87f351faeb7be30b", null ],
    [ "ATTR_GROUP", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a6c09ad20945eca169d2abd239e258c1a.html#a6c09ad20945eca169d2abd239e258c1a", null ],
    [ "ATTR_RESOURCE", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a2614be2c83298fdef466896a62f9436f.html#a2614be2c83298fdef466896a62f9436f", null ],
    [ "ATTR_ROLE", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a22e0b5d98dcbeb9dc0fdcb6e04fb1f67.html#a22e0b5d98dcbeb9dc0fdcb6e04fb1f67", null ],
    [ "EXTENSION_URI", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a15685f14f18096ea07c9ae421857c87c.html#a15685f14f18096ea07c9ae421857c87c", null ],
    [ "KEY_GROUP", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_ad4d7223bfa94f30c586a930194ebb61f.html#ad4d7223bfa94f30c586a930194ebb61f", null ],
    [ "KEY_RESOURCE", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_aeaa154c1aad255b728f97b345f6025a0.html#aeaa154c1aad255b728f97b345f6025a0", null ],
    [ "KEY_ROLE", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_a9f4ae493b7069d304c7e03e424e2902d.html#a9f4ae493b7069d304c7e03e424e2902d", null ],
    [ "Instance", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension_afd0e48683eaaf9253b0ceb836d027fe3.html#afd0e48683eaaf9253b0ceb836d027fe3", null ]
];