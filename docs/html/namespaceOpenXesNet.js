var namespaceOpenXesNet =
[
    [ "classification", "namespaceOpenXesNet_1_1classification.html", "namespaceOpenXesNet_1_1classification" ],
    [ "extension", "namespaceOpenXesNet_1_1extension.html", "namespaceOpenXesNet_1_1extension" ],
    [ "factory", "namespaceOpenXesNet_1_1factory.html", "namespaceOpenXesNet_1_1factory" ],
    [ "id", "namespaceOpenXesNet_1_1id.html", "namespaceOpenXesNet_1_1id" ],
    [ "info", "namespaceOpenXesNet_1_1info.html", "namespaceOpenXesNet_1_1info" ],
    [ "io", "namespaceOpenXesNet_1_1io.html", "namespaceOpenXesNet_1_1io" ],
    [ "logging", "namespaceOpenXesNet_1_1logging.html", "namespaceOpenXesNet_1_1logging" ],
    [ "model", "namespaceOpenXesNet_1_1model.html", "namespaceOpenXesNet_1_1model" ],
    [ "util", "namespaceOpenXesNet_1_1util.html", "namespaceOpenXesNet_1_1util" ]
];