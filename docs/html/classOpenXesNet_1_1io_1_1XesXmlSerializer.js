var classOpenXesNet_1_1io_1_1XesXmlSerializer =
[
    [ "AddAttributes", "classOpenXesNet_1_1io_1_1XesXmlSerializer_af0b3a790677b231eb16cb0152003116d.html#af0b3a790677b231eb16cb0152003116d", null ],
    [ "Serialize", "classOpenXesNet_1_1io_1_1XesXmlSerializer_a5c1476e6855c299abe7b8cdda6dc83f9.html#a5c1476e6855c299abe7b8cdda6dc83f9", null ],
    [ "ToString", "classOpenXesNet_1_1io_1_1XesXmlSerializer_ade3e4a5003a20d15cc0fac4469585a37.html#ade3e4a5003a20d15cc0fac4469585a37", null ],
    [ "Author", "classOpenXesNet_1_1io_1_1XesXmlSerializer_ad7f19bae5803bcc704c658dd6b4ce51b.html#ad7f19bae5803bcc704c658dd6b4ce51b", null ],
    [ "Description", "classOpenXesNet_1_1io_1_1XesXmlSerializer_a0dd6f9ae4513f7877589c9843f367e14.html#a0dd6f9ae4513f7877589c9843f367e14", null ],
    [ "Name", "classOpenXesNet_1_1io_1_1XesXmlSerializer_aa930dc8c869daec0ae96b891df8a5086.html#aa930dc8c869daec0ae96b891df8a5086", null ],
    [ "Suffices", "classOpenXesNet_1_1io_1_1XesXmlSerializer_a537857490c56c3ea6d4f68e2bbbfce85.html#a537857490c56c3ea6d4f68e2bbbfce85", null ]
];