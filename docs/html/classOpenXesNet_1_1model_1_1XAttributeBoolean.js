var classOpenXesNet_1_1model_1_1XAttributeBoolean =
[
    [ "XAttributeBoolean", "classOpenXesNet_1_1model_1_1XAttributeBoolean_a64cc2a35a0361f766742cf7518b95c90.html#a64cc2a35a0361f766742cf7518b95c90", null ],
    [ "XAttributeBoolean", "classOpenXesNet_1_1model_1_1XAttributeBoolean_a4fc017775a5cc5ec3c7d152ae48e72bc.html#a4fc017775a5cc5ec3c7d152ae48e72bc", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XAttributeBoolean_a4be3febd0066850c545b237f6f7ee024.html#a4be3febd0066850c545b237f6f7ee024", null ],
    [ "CompareTo", "classOpenXesNet_1_1model_1_1XAttributeBoolean_ab2390ecbfff84af27eaaf30de015edd5.html#ab2390ecbfff84af27eaaf30de015edd5", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XAttributeBoolean_a4c1f7853bd955d519c0a09749bfc66d9.html#a4c1f7853bd955d519c0a09749bfc66d9", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XAttributeBoolean_ada80cc02cd220d9ed3b31453e6a64d3f.html#ada80cc02cd220d9ed3b31453e6a64d3f", null ],
    [ "ToString", "classOpenXesNet_1_1model_1_1XAttributeBoolean_ae9fa72929d522e884473cdd2994bf782.html#ae9fa72929d522e884473cdd2994bf782", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttributeBoolean_a69121d0fc61fee08c9d420a422385413.html#a69121d0fc61fee08c9d420a422385413", null ]
];