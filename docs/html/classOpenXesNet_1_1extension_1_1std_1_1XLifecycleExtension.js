var classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension =
[
    [ "StandardModel", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513f", [
      [ "SCHEDULE", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513faecb7e253d295f3bbbfe12e491c9b7120", null ],
      [ "ASSIGN", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513faffd6976a2b4f6934eb075d0013316ff1", null ],
      [ "WITHDRAW", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fad46cbcb197473899801adc52f1d94deb", null ],
      [ "REASSIGN", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fa4407d3ac8900fc332e63a301c332499c", null ],
      [ "START", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fab078ffd28db767c502ac367053f6e0ac", null ],
      [ "SUSPEND", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513faee93a0b023cef18b34ebfee347881c14", null ],
      [ "RESUME", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fabac2aec3ee8d7d495123dbe5ca2fdac9", null ],
      [ "PI_ABORT", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fa86b4c4b5e6df1e0d37351ab3d601978a", null ],
      [ "ATE_ABORT", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513faf59dba6cd168439a6a5ba14826207dec", null ],
      [ "COMPLETE", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fa3de44296982e58199afc513a715b12ba", null ],
      [ "AUTOSKIP", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513faaaed485abe5c3f716d74b12464f1170a", null ],
      [ "MANUALSKIP", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fae4b841653c2c80acce5039bae0643096", null ],
      [ "UNKNOWN", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6eab248bd758e814a1a796ca2fd2513f.html#a6eab248bd758e814a1a796ca2fd2513fa696b031073e74bf2cb98e5ef201d4aa3", null ]
    ] ],
    [ "AssignModel", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a9ae5bbfc1d8a733144e7881adc17438a.html#a9ae5bbfc1d8a733144e7881adc17438a", null ],
    [ "AssignStandardTransition", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a31c830893123bec7ed518bf552c212d0.html#a31c830893123bec7ed518bf552c212d0", null ],
    [ "AssignTransition", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a2ab2d1faab931acb57565bfe12a040ff.html#a2ab2d1faab931acb57565bfe12a040ff", null ],
    [ "Decode", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_aa54bd1043f28a4c76fd9da2c7e562a8d.html#aa54bd1043f28a4c76fd9da2c7e562a8d", null ],
    [ "ExtractModel", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_aaf13d818dd7d868ba84f067f937ba7d3.html#aaf13d818dd7d868ba84f067f937ba7d3", null ],
    [ "ExtractStandardTransition", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_aa3abc59a5b26b317e86993a8986cb35f.html#aa3abc59a5b26b317e86993a8986cb35f", null ],
    [ "ExtractTransition", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a1abc77141e935c4024efd805cfe416e8.html#a1abc77141e935c4024efd805cfe416e8", null ],
    [ "UsesStandardModel", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_aa1cc57fbf8d40b7ec7c95e3ec04897b3.html#aa1cc57fbf8d40b7ec7c95e3ec04897b3", null ],
    [ "ATTR_MODEL", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a05de958f8f31f5d9e3660a72ea8534c6.html#a05de958f8f31f5d9e3660a72ea8534c6", null ],
    [ "ATTR_TRANSITION", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_afdb09ccc1b828cb471190b108426c13c.html#afdb09ccc1b828cb471190b108426c13c", null ],
    [ "EXTENSION_URI", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a7e505be2ab243abee3584075d52d70cd.html#a7e505be2ab243abee3584075d52d70cd", null ],
    [ "KEY_MODEL", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_aed9f6f15732cbfed9a9eee42e8054261.html#aed9f6f15732cbfed9a9eee42e8054261", null ],
    [ "KEY_TRANSITION", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a6f13a0cfae09fdf591f858ace0250091.html#a6f13a0cfae09fdf591f858ace0250091", null ],
    [ "VALUE_MODEL_STANDARD", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_a9dcbb34222d9cc686026f1b87cf83f39.html#a9dcbb34222d9cc686026f1b87cf83f39", null ],
    [ "Instance", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension_ad2d1312985d7a7571fa1d554b54ed79c.html#ad2d1312985d7a7571fa1d554b54ed79c", null ]
];