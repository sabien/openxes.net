var classOpenXesNet_1_1classification_1_1XEventAttributeClassifier =
[
    [ "XEventAttributeClassifier", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a56251fd352f1f5f0cda14cfff3b1f2d6.html#a56251fd352f1f5f0cda14cfff3b1f2d6", null ],
    [ "Accept", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a35d90daac45d342a050bd7e0d52e4439.html#a35d90daac45d342a050bd7e0d52e4439", null ],
    [ "CompareTo", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a9a472f3be743b5307dfe4294507313f0.html#a9a472f3be743b5307dfe4294507313f0", null ],
    [ "Equals", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a0475a8102a46a7b2031d1111fbf001a7.html#a0475a8102a46a7b2031d1111fbf001a7", null ],
    [ "GetClassIdentity", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a6bce05f83d764262d4f48cd4ec6e776e.html#a6bce05f83d764262d4f48cd4ec6e776e", null ],
    [ "GetHashCode", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a28fef874b3e0d6094fdffa590704a6b2.html#a28fef874b3e0d6094fdffa590704a6b2", null ],
    [ "SameEventClass", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a12c4fac1dcd389e3eb3909421bdde497.html#a12c4fac1dcd389e3eb3909421bdde497", null ],
    [ "ToString", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a7cbb296a9ca26028d0a1a5e1d89d2000.html#a7cbb296a9ca26028d0a1a5e1d89d2000", null ],
    [ "keys", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a5731164bea086d2245af2f89ae537ab4.html#a5731164bea086d2245af2f89ae537ab4", null ],
    [ "name", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_ac2f30423f2768a32da229eb5a861f54e.html#ac2f30423f2768a32da229eb5a861f54e", null ],
    [ "DefiningAttributeKeys", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a290139bfeb04d9c98bbe469b633d7bc6.html#a290139bfeb04d9c98bbe469b633d7bc6", null ],
    [ "DefiningAttributeKeysAsList", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a69fa426e8de66c4c2c10d5cd875a6bf1.html#a69fa426e8de66c4c2c10d5cd875a6bf1", null ],
    [ "Name", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier_a751144f57a3cba78641d399065ca9c21.html#a751144f57a3cba78641d399065ca9c21", null ]
];