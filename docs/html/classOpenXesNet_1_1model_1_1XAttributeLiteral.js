var classOpenXesNet_1_1model_1_1XAttributeLiteral =
[
    [ "XAttributeLiteral", "classOpenXesNet_1_1model_1_1XAttributeLiteral_a0db64464fa2bf7a3de527f219db6e064.html#a0db64464fa2bf7a3de527f219db6e064", null ],
    [ "XAttributeLiteral", "classOpenXesNet_1_1model_1_1XAttributeLiteral_a0f5b8410b26153f1d5d6c43c52517c0c.html#a0f5b8410b26153f1d5d6c43c52517c0c", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XAttributeLiteral_ad80e05af32e34d5e4a3554dffacecd97.html#ad80e05af32e34d5e4a3554dffacecd97", null ],
    [ "CompareTo", "classOpenXesNet_1_1model_1_1XAttributeLiteral_ae35159a50de5b9db5ef1ba75ab181ef8.html#ae35159a50de5b9db5ef1ba75ab181ef8", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XAttributeLiteral_a876e3b5621d7740595b1e915300439a8.html#a876e3b5621d7740595b1e915300439a8", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XAttributeLiteral_a5478e37d847a0004c4b17686c208005d.html#a5478e37d847a0004c4b17686c208005d", null ],
    [ "ToString", "classOpenXesNet_1_1model_1_1XAttributeLiteral_a08cdbecea2c7683ae80baebf9105948a.html#a08cdbecea2c7683ae80baebf9105948a", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttributeLiteral_a5ab26c165b2808080687afd6837bcac4.html#a5ab26c165b2808080687afd6837bcac4", null ]
];