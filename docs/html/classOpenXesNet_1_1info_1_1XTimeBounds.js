var classOpenXesNet_1_1info_1_1XTimeBounds =
[
    [ "XTimeBounds", "classOpenXesNet_1_1info_1_1XTimeBounds_a5decc0aa2a798be9b5f384918afcb34d.html#a5decc0aa2a798be9b5f384918afcb34d", null ],
    [ "GetEndDate", "classOpenXesNet_1_1info_1_1XTimeBounds_a3219fb3fceb0c1660e8774044a8383d3.html#a3219fb3fceb0c1660e8774044a8383d3", null ],
    [ "GetStartDate", "classOpenXesNet_1_1info_1_1XTimeBounds_a6527333fb57e6123c1972b6f927a292b.html#a6527333fb57e6123c1972b6f927a292b", null ],
    [ "IsWithin", "classOpenXesNet_1_1info_1_1XTimeBounds_a548c9bbbf6e7f4fb9d5a8d4764b06175.html#a548c9bbbf6e7f4fb9d5a8d4764b06175", null ],
    [ "Register", "classOpenXesNet_1_1info_1_1XTimeBounds_af68a02273d9473f96fbf2a9c67da8268.html#af68a02273d9473f96fbf2a9c67da8268", null ],
    [ "Register", "classOpenXesNet_1_1info_1_1XTimeBounds_a652550d7bbc79a1726f3ea8452f6ab31.html#a652550d7bbc79a1726f3ea8452f6ab31", null ],
    [ "Register", "classOpenXesNet_1_1info_1_1XTimeBounds_a211c3c0eeffa241d85d0232f7e5ec19e.html#a211c3c0eeffa241d85d0232f7e5ec19e", null ],
    [ "ToString", "classOpenXesNet_1_1info_1_1XTimeBounds_a5812d8c3d4933caa330c3b8ca228640c.html#a5812d8c3d4933caa330c3b8ca228640c", null ],
    [ "first", "classOpenXesNet_1_1info_1_1XTimeBounds_aea56d2aee11a990baacd04959855bfa9.html#aea56d2aee11a990baacd04959855bfa9", null ],
    [ "last", "classOpenXesNet_1_1info_1_1XTimeBounds_af1e70fb16e431e34ef2ce1a42ed57e42.html#af1e70fb16e431e34ef2ce1a42ed57e42", null ]
];