var hierarchy =
[
    [ "Dictionary", null, [
      [ "OpenXesNet.info.XAttributeNameMap", "classOpenXesNet_1_1info_1_1XAttributeNameMap.html", null ],
      [ "OpenXesNet.info.XGlobalAttributeNameMap", "classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap.html", null ],
      [ "OpenXesNet.model.XAttributeMap", "classOpenXesNet_1_1model_1_1XAttributeMap.html", null ]
    ] ],
    [ "ICloneable", null, [
      [ "OpenXesNet.id.XID", "classOpenXesNet_1_1id_1_1XID.html", null ],
      [ "OpenXesNet.model.IXAttribute< T >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", [
        [ "OpenXesNet.model.XAttribute< T >", "classOpenXesNet_1_1model_1_1XAttribute.html", [
          [ "OpenXesNet.model.XAttributeBoolean", "classOpenXesNet_1_1model_1_1XAttributeBoolean.html", null ],
          [ "OpenXesNet.model.XAttributeContinuous", "classOpenXesNet_1_1model_1_1XAttributeContinuous.html", null ],
          [ "OpenXesNet.model.XAttributeDiscrete", "classOpenXesNet_1_1model_1_1XAttributeDiscrete.html", null ],
          [ "OpenXesNet.model.XAttributeID", "classOpenXesNet_1_1model_1_1XAttributeID.html", null ],
          [ "OpenXesNet.model.XAttributeLiteral", "classOpenXesNet_1_1model_1_1XAttributeLiteral.html", [
            [ "OpenXesNet.model.XAttributeCollection", "classOpenXesNet_1_1model_1_1XAttributeCollection.html", [
              [ "OpenXesNet.model.XAttributeContainer", "classOpenXesNet_1_1model_1_1XAttributeContainer.html", null ],
              [ "OpenXesNet.model.XAttributeList", "classOpenXesNet_1_1model_1_1XAttributeList.html", null ]
            ] ]
          ] ],
          [ "OpenXesNet.model.XAttributeTimestamp", "classOpenXesNet_1_1model_1_1XAttributeTimestamp.html", null ]
        ] ]
      ] ],
      [ "OpenXesNet.model.IXAttributeMap", "interfaceOpenXesNet_1_1model_1_1IXAttributeMap.html", [
        [ "OpenXesNet.model.XAttributeMap", "classOpenXesNet_1_1model_1_1XAttributeMap.html", null ],
        [ "OpenXesNet.model.XAttributeMapLazy< T >", "classOpenXesNet_1_1model_1_1XAttributeMapLazy.html", null ]
      ] ],
      [ "OpenXesNet.model.IXElement", "interfaceOpenXesNet_1_1model_1_1IXElement.html", [
        [ "OpenXesNet.model.IXEvent", "interfaceOpenXesNet_1_1model_1_1IXEvent.html", [
          [ "OpenXesNet.model.XEvent", "classOpenXesNet_1_1model_1_1XEvent.html", null ]
        ] ],
        [ "OpenXesNet.model.IXLog", "interfaceOpenXesNet_1_1model_1_1IXLog.html", [
          [ "OpenXesNet.model.XLog", "classOpenXesNet_1_1model_1_1XLog.html", null ]
        ] ],
        [ "OpenXesNet.model.IXTrace", "interfaceOpenXesNet_1_1model_1_1IXTrace.html", [
          [ "OpenXesNet.model.XTrace", "classOpenXesNet_1_1model_1_1XTrace.html", null ]
        ] ]
      ] ]
    ] ],
    [ "IComparable", null, [
      [ "OpenXesNet.classification.XEventAttributeClassifier", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier.html", [
        [ "OpenXesNet.classification.XEventAndClassifier", "classOpenXesNet_1_1classification_1_1XEventAndClassifier.html", null ],
        [ "OpenXesNet.classification.XEventLifeTransClassifier", "classOpenXesNet_1_1classification_1_1XEventLifeTransClassifier.html", null ],
        [ "OpenXesNet.classification.XEventNameClassifier", "classOpenXesNet_1_1classification_1_1XEventNameClassifier.html", null ],
        [ "OpenXesNet.classification.XEventResourceClassifier", "classOpenXesNet_1_1classification_1_1XEventResourceClassifier.html", null ]
      ] ],
      [ "OpenXesNet.classification.XEventClass", "classOpenXesNet_1_1classification_1_1XEventClass.html", null ],
      [ "OpenXesNet.id.XID", "classOpenXesNet_1_1id_1_1XID.html", null ],
      [ "OpenXesNet.model.IXAttribute< T >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", null ]
    ] ],
    [ "IDictionary", null, [
      [ "OpenXesNet.model.IXAttributeMap", "interfaceOpenXesNet_1_1model_1_1IXAttributeMap.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "OpenXesNet.io.XParser", "classOpenXesNet_1_1io_1_1XParser.html", [
        [ "OpenXesNet.io.XesXmlParser", "classOpenXesNet_1_1io_1_1XesXmlParser.html", [
          [ "OpenXesNet.io.XesXmlGzipParser", "classOpenXesNet_1_1io_1_1XesXmlGzipParser.html", null ]
        ] ]
      ] ]
    ] ],
    [ "IList", null, [
      [ "OpenXesNet.model.IXLog", "interfaceOpenXesNet_1_1model_1_1IXLog.html", null ],
      [ "OpenXesNet.model.IXTrace", "interfaceOpenXesNet_1_1model_1_1IXTrace.html", null ]
    ] ],
    [ "OpenXesNet.model.IXAttributable", "interfaceOpenXesNet_1_1model_1_1IXAttributable.html", [
      [ "OpenXesNet.model.IXAttribute< T >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", null ],
      [ "OpenXesNet.model.IXElement", "interfaceOpenXesNet_1_1model_1_1IXElement.html", null ]
    ] ],
    [ "OpenXesNet.model.IXAttribute< bool >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", [
      [ "OpenXesNet.model.IXAttributeBoolean", "interfaceOpenXesNet_1_1model_1_1IXAttributeBoolean.html", [
        [ "OpenXesNet.model.XAttributeBoolean", "classOpenXesNet_1_1model_1_1XAttributeBoolean.html", null ]
      ] ]
    ] ],
    [ "OpenXesNet.model.IXAttribute< DateTime >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", [
      [ "OpenXesNet.model.IXAttributeTimestamp", "interfaceOpenXesNet_1_1model_1_1IXAttributeTimestamp.html", [
        [ "OpenXesNet.model.XAttributeTimestamp", "classOpenXesNet_1_1model_1_1XAttributeTimestamp.html", null ]
      ] ]
    ] ],
    [ "OpenXesNet.model.IXAttribute< double >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", [
      [ "OpenXesNet.model.IXAttributeContinuous", "interfaceOpenXesNet_1_1model_1_1IXAttributeContinuous.html", [
        [ "OpenXesNet.model.XAttributeContinuous", "classOpenXesNet_1_1model_1_1XAttributeContinuous.html", null ]
      ] ]
    ] ],
    [ "IXAttribute< List< XAttribute >>", null, [
      [ "OpenXesNet.model.IXAttributeCollection", "interfaceOpenXesNet_1_1model_1_1IXAttributeCollection.html", [
        [ "OpenXesNet.model.IXAttributeContainer", "interfaceOpenXesNet_1_1model_1_1IXAttributeContainer.html", [
          [ "OpenXesNet.model.XAttributeContainer", "classOpenXesNet_1_1model_1_1XAttributeContainer.html", null ]
        ] ],
        [ "OpenXesNet.model.IXAttributeList", "interfaceOpenXesNet_1_1model_1_1IXAttributeList.html", [
          [ "OpenXesNet.model.XAttributeList", "classOpenXesNet_1_1model_1_1XAttributeList.html", null ]
        ] ],
        [ "OpenXesNet.model.XAttributeCollection", "classOpenXesNet_1_1model_1_1XAttributeCollection.html", null ]
      ] ]
    ] ],
    [ "OpenXesNet.model.IXAttribute< long >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", [
      [ "OpenXesNet.model.IXAttributeDiscrete", "interfaceOpenXesNet_1_1model_1_1IXAttributeDiscrete.html", [
        [ "OpenXesNet.model.XAttributeDiscrete", "classOpenXesNet_1_1model_1_1XAttributeDiscrete.html", null ]
      ] ]
    ] ],
    [ "OpenXesNet.model.IXAttribute< string >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", [
      [ "OpenXesNet.model.IXAttributeLiteral", "interfaceOpenXesNet_1_1model_1_1IXAttributeLiteral.html", [
        [ "OpenXesNet.model.XAttributeLiteral", "classOpenXesNet_1_1model_1_1XAttributeLiteral.html", null ]
      ] ]
    ] ],
    [ "OpenXesNet.model.IXAttribute< XID >", "interfaceOpenXesNet_1_1model_1_1IXAttribute.html", [
      [ "OpenXesNet.model.IXAttributeID", "interfaceOpenXesNet_1_1model_1_1IXAttributeID.html", [
        [ "OpenXesNet.model.XAttributeID", "classOpenXesNet_1_1model_1_1XAttributeID.html", null ]
      ] ]
    ] ],
    [ "OpenXesNet.info.IXAttributeInfo", "interfaceOpenXesNet_1_1info_1_1IXAttributeInfo.html", [
      [ "OpenXesNet.info.XAttributeInfo", "classOpenXesNet_1_1info_1_1XAttributeInfo.html", null ]
    ] ],
    [ "OpenXesNet.info.IXAttributeNameMap", "interfaceOpenXesNet_1_1info_1_1IXAttributeNameMap.html", [
      [ "OpenXesNet.info.XAttributeNameMap", "classOpenXesNet_1_1info_1_1XAttributeNameMap.html", null ],
      [ "OpenXesNet.info.XGlobalAttributeNameMap", "classOpenXesNet_1_1info_1_1XGlobalAttributeNameMap.html", null ]
    ] ],
    [ "OpenXesNet.classification.IXEventClassifier", "interfaceOpenXesNet_1_1classification_1_1IXEventClassifier.html", [
      [ "OpenXesNet.classification.XEventAttributeClassifier", "classOpenXesNet_1_1classification_1_1XEventAttributeClassifier.html", null ]
    ] ],
    [ "OpenXesNet.factory.IXFactory", "interfaceOpenXesNet_1_1factory_1_1IXFactory.html", [
      [ "OpenXesNet.factory.XFactoryNaive", "classOpenXesNet_1_1factory_1_1XFactoryNaive.html", null ]
    ] ],
    [ "OpenXesNet.logging.IXLoggingListener", "interfaceOpenXesNet_1_1logging_1_1IXLoggingListener.html", [
      [ "OpenXesNet.logging.XStdOutLoggingListener", "classOpenXesNet_1_1logging_1_1XStdOutLoggingListener.html", null ]
    ] ],
    [ "OpenXesNet.info.IXLogInfo", "interfaceOpenXesNet_1_1info_1_1IXLogInfo.html", [
      [ "OpenXesNet.info.XLogInfo", "classOpenXesNet_1_1info_1_1XLogInfo.html", null ]
    ] ],
    [ "OpenXesNet.io.IXSerializer", "interfaceOpenXesNet_1_1io_1_1IXSerializer.html", [
      [ "OpenXesNet.io.XesXmlSerializer", "classOpenXesNet_1_1io_1_1XesXmlSerializer.html", [
        [ "OpenXesNet.io.XesXmlGZIPSerializer", "classOpenXesNet_1_1io_1_1XesXmlGZIPSerializer.html", null ]
      ] ]
    ] ],
    [ "OpenXesNet.info.IXTimeBounds", "interfaceOpenXesNet_1_1info_1_1IXTimeBounds.html", [
      [ "OpenXesNet.info.XTimeBounds", "classOpenXesNet_1_1info_1_1XTimeBounds.html", null ]
    ] ],
    [ "List", null, [
      [ "OpenXesNet.model.XLog", "classOpenXesNet_1_1model_1_1XLog.html", null ],
      [ "OpenXesNet.model.XTrace", "classOpenXesNet_1_1model_1_1XTrace.html", null ]
    ] ],
    [ "OpenXesNet.extension.std.XAbstractNestedAttributeSupport< T >", "classOpenXesNet_1_1extension_1_1std_1_1XAbstractNestedAttributeSupport.html", null ],
    [ "OpenXesNet.extension.std.XAbstractNestedAttributeSupport< double >", "classOpenXesNet_1_1extension_1_1std_1_1XAbstractNestedAttributeSupport.html", [
      [ "OpenXesNet.extension.std.cost.XCostAmount", "classOpenXesNet_1_1extension_1_1std_1_1cost_1_1XCostAmount.html", null ]
    ] ],
    [ "OpenXesNet.extension.std.XAbstractNestedAttributeSupport< String >", "classOpenXesNet_1_1extension_1_1std_1_1XAbstractNestedAttributeSupport.html", [
      [ "OpenXesNet.extension.std.cost.XCostDriver", "classOpenXesNet_1_1extension_1_1std_1_1cost_1_1XCostDriver.html", null ]
    ] ],
    [ "OpenXesNet.extension.std.XAbstractNestedAttributeSupport< string >", "classOpenXesNet_1_1extension_1_1std_1_1XAbstractNestedAttributeSupport.html", [
      [ "OpenXesNet.extension.std.cost.XCostType", "classOpenXesNet_1_1extension_1_1std_1_1cost_1_1XCostType.html", null ]
    ] ],
    [ "XAttribute", null, [
      [ "OpenXesNet.model.XAttribute< T >", "classOpenXesNet_1_1model_1_1XAttribute.html", null ]
    ] ],
    [ "OpenXesNet.util.XAttributeUtils", "classOpenXesNet_1_1util_1_1XAttributeUtils.html", null ],
    [ "OpenXesNet.classification.XEventClasses", "classOpenXesNet_1_1classification_1_1XEventClasses.html", null ],
    [ "OpenXesNet.extension.XExtension", "classOpenXesNet_1_1extension_1_1XExtension.html", [
      [ "OpenXesNet.extension.std.XConceptExtension", "classOpenXesNet_1_1extension_1_1std_1_1XConceptExtension.html", null ],
      [ "OpenXesNet.extension.std.XCostExtension", "classOpenXesNet_1_1extension_1_1std_1_1XCostExtension.html", null ],
      [ "OpenXesNet.extension.std.XIdentityExtension", "classOpenXesNet_1_1extension_1_1std_1_1XIdentityExtension.html", null ],
      [ "OpenXesNet.extension.std.XLifecycleExtension", "classOpenXesNet_1_1extension_1_1std_1_1XLifecycleExtension.html", null ],
      [ "OpenXesNet.extension.std.XMicroExtension", "classOpenXesNet_1_1extension_1_1std_1_1XMicroExtension.html", null ],
      [ "OpenXesNet.extension.std.XOrganizationalExtension", "classOpenXesNet_1_1extension_1_1std_1_1XOrganizationalExtension.html", null ],
      [ "OpenXesNet.extension.std.XSemanticExtension", "classOpenXesNet_1_1extension_1_1std_1_1XSemanticExtension.html", null ],
      [ "OpenXesNet.extension.std.XSoftwareCommunicationExtension", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension.html", null ],
      [ "OpenXesNet.extension.std.XSoftwareEventExtension", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareEventExtension.html", null ],
      [ "OpenXesNet.extension.std.XSoftwareTelemetryExtension", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareTelemetryExtension.html", null ],
      [ "OpenXesNet.extension.std.XTimeExtension", "classOpenXesNet_1_1extension_1_1std_1_1XTimeExtension.html", null ]
    ] ],
    [ "OpenXesNet.extension.XExtensionManager", "classOpenXesNet_1_1extension_1_1XExtensionManager.html", null ],
    [ "OpenXesNet.extension.XExtensionParser", "classOpenXesNet_1_1extension_1_1XExtensionParser.html", null ],
    [ "OpenXesNet.id.XIDFactory", "classOpenXesNet_1_1id_1_1XIDFactory.html", null ],
    [ "OpenXesNet.logging.XLogging", "classOpenXesNet_1_1logging_1_1XLogging.html", null ],
    [ "OpenXesNet.info.XLogInfoFactory", "classOpenXesNet_1_1info_1_1XLogInfoFactory.html", null ],
    [ "OpenXesNet.util.XRegistry< T >", "classOpenXesNet_1_1util_1_1XRegistry.html", null ],
    [ "OpenXesNet.util.XRegistry< IXFactory >", "classOpenXesNet_1_1util_1_1XRegistry.html", [
      [ "OpenXesNet.factory.XFactoryRegistry", "classOpenXesNet_1_1factory_1_1XFactoryRegistry.html", null ]
    ] ],
    [ "OpenXesNet.util.XRegistry< IXSerializer >", "classOpenXesNet_1_1util_1_1XRegistry.html", [
      [ "OpenXesNet.io.XSerializerRegistry", "classOpenXesNet_1_1io_1_1XSerializerRegistry.html", null ]
    ] ],
    [ "OpenXesNet.util.XRegistry< XParser >", "classOpenXesNet_1_1util_1_1XRegistry.html", [
      [ "OpenXesNet.io.XParserRegistry", "classOpenXesNet_1_1io_1_1XParserRegistry.html", null ]
    ] ],
    [ "OpenXesNet.util.XRuntimeUtils", "classOpenXesNet_1_1util_1_1XRuntimeUtils.html", null ],
    [ "OpenXesNet.util.XTokenHelper", "classOpenXesNet_1_1util_1_1XTokenHelper.html", null ],
    [ "OpenXesNet.io.XUniversalParser", "classOpenXesNet_1_1io_1_1XUniversalParser.html", null ],
    [ "OpenXesNet.model.XVisitor", "classOpenXesNet_1_1model_1_1XVisitor.html", null ]
];