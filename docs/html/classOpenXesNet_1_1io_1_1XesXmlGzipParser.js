var classOpenXesNet_1_1io_1_1XesXmlGzipParser =
[
    [ "XesXmlGzipParser", "classOpenXesNet_1_1io_1_1XesXmlGzipParser_a92209dee1fdf0400a37bc99a1ef0c5d3.html#a92209dee1fdf0400a37bc99a1ef0c5d3", null ],
    [ "XesXmlGzipParser", "classOpenXesNet_1_1io_1_1XesXmlGzipParser_ac82b68ec2ed1d6190576b0ca79ae5a6f.html#ac82b68ec2ed1d6190576b0ca79ae5a6f", null ],
    [ "CanParse", "classOpenXesNet_1_1io_1_1XesXmlGzipParser_a1d5695b8cbf4d977cd4282d1807077f7.html#a1d5695b8cbf4d977cd4282d1807077f7", null ],
    [ "Parse", "classOpenXesNet_1_1io_1_1XesXmlGzipParser_a1bdd44a1ce6a2fa80b0d09ab15c93b6f.html#a1bdd44a1ce6a2fa80b0d09ab15c93b6f", null ],
    [ "Parse", "classOpenXesNet_1_1io_1_1XesXmlGzipParser_a07e5253519b263097c722490d7160f2c.html#a07e5253519b263097c722490d7160f2c", null ],
    [ "Description", "classOpenXesNet_1_1io_1_1XesXmlGzipParser_a195e49c13d8cd8433f0097db9c8fc4e8.html#a195e49c13d8cd8433f0097db9c8fc4e8", null ],
    [ "Name", "classOpenXesNet_1_1io_1_1XesXmlGzipParser_a1a27739477d3949a36b53e17a266cf33.html#a1a27739477d3949a36b53e17a266cf33", null ]
];