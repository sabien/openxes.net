var classOpenXesNet_1_1model_1_1XLog =
[
    [ "XLog", "classOpenXesNet_1_1model_1_1XLog_a85124e13e99297fed30161774ea44103.html#a85124e13e99297fed30161774ea44103", null ],
    [ "Accept", "classOpenXesNet_1_1model_1_1XLog_acfd04726afbe2527bd73c9e8b3d478b2.html#acfd04726afbe2527bd73c9e8b3d478b2", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XLog_a82f683af35ad45c4469953790051f8d7.html#a82f683af35ad45c4469953790051f8d7", null ],
    [ "GetAttributes", "classOpenXesNet_1_1model_1_1XLog_a54e4d41e5a854520100214e92b76d151.html#a54e4d41e5a854520100214e92b76d151", null ],
    [ "GetInfo", "classOpenXesNet_1_1model_1_1XLog_a689b5403210b9c9cdf2947666f35a2ce.html#a689b5403210b9c9cdf2947666f35a2ce", null ],
    [ "HasAttributes", "classOpenXesNet_1_1model_1_1XLog_ad294ede6d5cf6ae4193e4294e4cba092.html#ad294ede6d5cf6ae4193e4294e4cba092", null ],
    [ "SetAttributes", "classOpenXesNet_1_1model_1_1XLog_a7764e4089c73e2e3c658853d8d0f2556.html#a7764e4089c73e2e3c658853d8d0f2556", null ],
    [ "SetInfo", "classOpenXesNet_1_1model_1_1XLog_a62e7ee5e6c2b595ad0c682e7f1a4e49c.html#a62e7ee5e6c2b595ad0c682e7f1a4e49c", null ],
    [ "Classifiers", "classOpenXesNet_1_1model_1_1XLog_abc25f189cc1b681a5aaeaa31204cb1dc.html#abc25f189cc1b681a5aaeaa31204cb1dc", null ],
    [ "Extensions", "classOpenXesNet_1_1model_1_1XLog_a73d17e31f90590dfacbbc1e98e72d671.html#a73d17e31f90590dfacbbc1e98e72d671", null ],
    [ "Features", "classOpenXesNet_1_1model_1_1XLog_aad8c54e9de7d36c27afc2b089f4802bd.html#aad8c54e9de7d36c27afc2b089f4802bd", null ],
    [ "GlobalEventAttributes", "classOpenXesNet_1_1model_1_1XLog_ab9ebda319c8733e3db6e91498e9e1c26.html#ab9ebda319c8733e3db6e91498e9e1c26", null ],
    [ "GlobalTraceAttributes", "classOpenXesNet_1_1model_1_1XLog_a98c86b4ae54fd9c7db0a8ceb8aa9f6a2.html#a98c86b4ae54fd9c7db0a8ceb8aa9f6a2", null ],
    [ "Version", "classOpenXesNet_1_1model_1_1XLog_a8692084c3a05c27485d296e45dfa2824.html#a8692084c3a05c27485d296e45dfa2824", null ]
];