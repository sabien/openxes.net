var classOpenXesNet_1_1model_1_1XAttributeContinuous =
[
    [ "XAttributeContinuous", "classOpenXesNet_1_1model_1_1XAttributeContinuous_a9c085398a44d21cd4a21421abceb7add.html#a9c085398a44d21cd4a21421abceb7add", null ],
    [ "XAttributeContinuous", "classOpenXesNet_1_1model_1_1XAttributeContinuous_aec952e551ce7aa044e5af20113ac7d62.html#aec952e551ce7aa044e5af20113ac7d62", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XAttributeContinuous_a8f5c84811466089c7f529dd318a81138.html#a8f5c84811466089c7f529dd318a81138", null ],
    [ "CompareTo", "classOpenXesNet_1_1model_1_1XAttributeContinuous_adc9b5691f156c29f763e2aa9e027164c.html#adc9b5691f156c29f763e2aa9e027164c", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XAttributeContinuous_acb3ebbd900abdad3e5f1a0c76f722ae0.html#acb3ebbd900abdad3e5f1a0c76f722ae0", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XAttributeContinuous_a9a5272521156a08dd176718f31416507.html#a9a5272521156a08dd176718f31416507", null ],
    [ "ToString", "classOpenXesNet_1_1model_1_1XAttributeContinuous_aa89444172d2e35f41fb3b1b757118c12.html#aa89444172d2e35f41fb3b1b757118c12", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttributeContinuous_aa86efb7718da91d091593161b189e8a4.html#aa86efb7718da91d091593161b189e8a4", null ]
];