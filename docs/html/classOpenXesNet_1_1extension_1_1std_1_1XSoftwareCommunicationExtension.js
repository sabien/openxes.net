var classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension =
[
    [ "AssignLocalHost", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_afa65bcf41a995a162c25f7969056892d.html#afa65bcf41a995a162c25f7969056892d", null ],
    [ "assignLocalPort", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_ac20420dba276f8980ed4623c71534bc9.html#ac20420dba276f8980ed4623c71534bc9", null ],
    [ "AssignRemoteHost", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_af1c2d49b924fc7d3312dcc66378c55f2.html#af1c2d49b924fc7d3312dcc66378c55f2", null ],
    [ "AssignRemotePort", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a1e45ded49bb3a9dc81cac628b2e9e073.html#a1e45ded49bb3a9dc81cac628b2e9e073", null ],
    [ "ExtractLocalHost", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a74e74e029c028ac5ac618c1f58c9a55e.html#a74e74e029c028ac5ac618c1f58c9a55e", null ],
    [ "ExtractLocalPort", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a8b83896e5b93a7dbbbb57fb111baf50f.html#a8b83896e5b93a7dbbbb57fb111baf50f", null ],
    [ "ExtractRemoteHost", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a3a9efaeb17bf1648165b02d0ecc5b8d2.html#a3a9efaeb17bf1648165b02d0ecc5b8d2", null ],
    [ "ExtractRemotePort", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a2a70189b45ef11f352badaa7ea1596c9.html#a2a70189b45ef11f352badaa7ea1596c9", null ],
    [ "ATTR_LOCAL_HOST", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_ac9657e054619013ff4d93c4b60dd69fd.html#ac9657e054619013ff4d93c4b60dd69fd", null ],
    [ "ATTR_LOCAL_PORT", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a8508ece492ac7a4c6f6f615834bc5ea1.html#a8508ece492ac7a4c6f6f615834bc5ea1", null ],
    [ "ATTR_REMOTE_HOST", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a5b5cf96813cf25ccaf7bb80e953ec05a.html#a5b5cf96813cf25ccaf7bb80e953ec05a", null ],
    [ "ATTR_REMOTE_PORT", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a4b9d33f664f5fa1f2b69b6936bb16b06.html#a4b9d33f664f5fa1f2b69b6936bb16b06", null ],
    [ "EXTENSION_URI", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a9115e4fe5010f27dd9c4f39503cfa96b.html#a9115e4fe5010f27dd9c4f39503cfa96b", null ],
    [ "KEY_LOCAL_HOST", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_aabac834065bafa4d461250fc055981dc.html#aabac834065bafa4d461250fc055981dc", null ],
    [ "KEY_LOCAL_PORT", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a98576705c0c7b204a74ef6d612212f27.html#a98576705c0c7b204a74ef6d612212f27", null ],
    [ "KEY_REMOTE_HOST", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_aa7f4ee4020723a9642531e836b392ca6.html#aa7f4ee4020723a9642531e836b392ca6", null ],
    [ "KEY_REMOTE_PORT", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_a127d1210feeedd4fcf0211502bc55bed.html#a127d1210feeedd4fcf0211502bc55bed", null ],
    [ "PREFIX", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_afbd64368ba8413b306d039bc0ff0de00.html#afbd64368ba8413b306d039bc0ff0de00", null ],
    [ "Instance", "classOpenXesNet_1_1extension_1_1std_1_1XSoftwareCommunicationExtension_ada1ec022ad41e1d8ffb33c7cf60f3e96.html#ada1ec022ad41e1d8ffb33c7cf60f3e96", null ]
];