var classOpenXesNet_1_1classification_1_1XEventClasses =
[
    [ "XEventClasses", "classOpenXesNet_1_1classification_1_1XEventClasses_affdd2ae930e170d1365bb6705fbc529e.html#affdd2ae930e170d1365bb6705fbc529e", null ],
    [ "DeriveEventClasses", "classOpenXesNet_1_1classification_1_1XEventClasses_a95dc6c3ad33825334018ed0a73533b68.html#a95dc6c3ad33825334018ed0a73533b68", null ],
    [ "Equals", "classOpenXesNet_1_1classification_1_1XEventClasses_a1ec700d1bc9071c093b5236ba3589c27.html#a1ec700d1bc9071c093b5236ba3589c27", null ],
    [ "GetByIdentity", "classOpenXesNet_1_1classification_1_1XEventClasses_a13934f467d3f3da28b05b40ea84b2175.html#a13934f467d3f3da28b05b40ea84b2175", null ],
    [ "GetByIndex", "classOpenXesNet_1_1classification_1_1XEventClasses_a2ab7831dc58335beff483cf3e04cde33.html#a2ab7831dc58335beff483cf3e04cde33", null ],
    [ "GetClassOf", "classOpenXesNet_1_1classification_1_1XEventClasses_a0f05bec1636591915f7814581c04123e.html#a0f05bec1636591915f7814581c04123e", null ],
    [ "GetHashCode", "classOpenXesNet_1_1classification_1_1XEventClasses_aa615585581f1dddde99e8cd9c78e0ae3.html#aa615585581f1dddde99e8cd9c78e0ae3", null ],
    [ "HarmonizeIndexes", "classOpenXesNet_1_1classification_1_1XEventClasses_a883668c2fff1a1ca36b8ed70775f8536.html#a883668c2fff1a1ca36b8ed70775f8536", null ],
    [ "Register", "classOpenXesNet_1_1classification_1_1XEventClasses_aee1e8e4f2f4891e75b594f0bf2e968ee.html#aee1e8e4f2f4891e75b594f0bf2e968ee", null ],
    [ "Register", "classOpenXesNet_1_1classification_1_1XEventClasses_a8851fb131877ccb587391ac7689f4003.html#a8851fb131877ccb587391ac7689f4003", null ],
    [ "Register", "classOpenXesNet_1_1classification_1_1XEventClasses_aa35f955f21a7733625544115ded3b903.html#aa35f955f21a7733625544115ded3b903", null ],
    [ "Register", "classOpenXesNet_1_1classification_1_1XEventClasses_a3f859d08c5d56050eda882751d9584df.html#a3f859d08c5d56050eda882751d9584df", null ],
    [ "ToString", "classOpenXesNet_1_1classification_1_1XEventClasses_acbd50a41fdef8110e9bc6fa2b33ef25c.html#acbd50a41fdef8110e9bc6fa2b33ef25c", null ],
    [ "classifier", "classOpenXesNet_1_1classification_1_1XEventClasses_ae4d68d937f34cae51000006570eb6c21.html#ae4d68d937f34cae51000006570eb6c21", null ],
    [ "classMap", "classOpenXesNet_1_1classification_1_1XEventClasses_a46ea2cc559a5763f2143a4da8ee3221f.html#a46ea2cc559a5763f2143a4da8ee3221f", null ],
    [ "Classes", "classOpenXesNet_1_1classification_1_1XEventClasses_ac0db0424c84111d1d507879b08ab06d1.html#ac0db0424c84111d1d507879b08ab06d1", null ],
    [ "Classifier", "classOpenXesNet_1_1classification_1_1XEventClasses_a2c70ba2a969499df375abd00a0adee2e.html#a2c70ba2a969499df375abd00a0adee2e", null ],
    [ "Count", "classOpenXesNet_1_1classification_1_1XEventClasses_a6fa39cff38f71aad10352c2c493fa7f0.html#a6fa39cff38f71aad10352c2c493fa7f0", null ]
];