var classOpenXesNet_1_1model_1_1XAttributeID =
[
    [ "XAttributeID", "classOpenXesNet_1_1model_1_1XAttributeID_a042f31c5a54f00d04e058a58174bb450.html#a042f31c5a54f00d04e058a58174bb450", null ],
    [ "XAttributeID", "classOpenXesNet_1_1model_1_1XAttributeID_afed51561677682e730d38bf9181bc08e.html#afed51561677682e730d38bf9181bc08e", null ],
    [ "Clone", "classOpenXesNet_1_1model_1_1XAttributeID_a22158e09a3c32a6193dd535c54e8a12d.html#a22158e09a3c32a6193dd535c54e8a12d", null ],
    [ "CompareTo", "classOpenXesNet_1_1model_1_1XAttributeID_a8d45697823de9316b6b8a5a5b07f6013.html#a8d45697823de9316b6b8a5a5b07f6013", null ],
    [ "Equals", "classOpenXesNet_1_1model_1_1XAttributeID_a42bcb159574e5167a027d869239ae98d.html#a42bcb159574e5167a027d869239ae98d", null ],
    [ "GetHashCode", "classOpenXesNet_1_1model_1_1XAttributeID_a3561d49689f4793f8d81740e327a97b9.html#a3561d49689f4793f8d81740e327a97b9", null ],
    [ "ToString", "classOpenXesNet_1_1model_1_1XAttributeID_ad8af0429564f042764aecf60ef9c2848.html#ad8af0429564f042764aecf60ef9c2848", null ],
    [ "Value", "classOpenXesNet_1_1model_1_1XAttributeID_a590bda989ecf6d3f7da57a8611385c4c.html#a590bda989ecf6d3f7da57a8611385c4c", null ]
];