var classOpenXesNet_1_1util_1_1XRegistry =
[
    [ "XRegistry", "classOpenXesNet_1_1util_1_1XRegistry_ae9a3b6eb25f7356d965033ae8b8e9a53.html#ae9a3b6eb25f7356d965033ae8b8e9a53", null ],
    [ "AreEqual", "classOpenXesNet_1_1util_1_1XRegistry_a3372e948dec067d91a6d34bc21151844.html#a3372e948dec067d91a6d34bc21151844", null ],
    [ "GetAvailable", "classOpenXesNet_1_1util_1_1XRegistry_adc65521decfb04bc4cff7f1dd6f5e864.html#adc65521decfb04bc4cff7f1dd6f5e864", null ],
    [ "IsContained", "classOpenXesNet_1_1util_1_1XRegistry_a62ec341b33704656b16ba7941f434a53.html#a62ec341b33704656b16ba7941f434a53", null ],
    [ "Register", "classOpenXesNet_1_1util_1_1XRegistry_a52dcedeb59ce4e3717ded86a8b44c54c.html#a52dcedeb59ce4e3717ded86a8b44c54c", null ],
    [ "CurrentDefault", "classOpenXesNet_1_1util_1_1XRegistry_af1860a44d455f13befc4f21e4705c6ad.html#af1860a44d455f13befc4f21e4705c6ad", null ]
];