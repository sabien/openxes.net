var interfaceOpenXesNet_1_1info_1_1IXLogInfo =
[
    [ "GetEventAttributeInfo", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_afd9847da79a6ff2eddf15dfdb4942b2e.html#afd9847da79a6ff2eddf15dfdb4942b2e", null ],
    [ "GetEventClasses", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_ae1608c511e76259520f098eaf776189c.html#ae1608c511e76259520f098eaf776189c", null ],
    [ "GetEventClasses", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a9ab6684154e08ac945d978c5cb471151.html#a9ab6684154e08ac945d978c5cb471151", null ],
    [ "GetEventClassifiers", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a4c7ba0535e6bd550bec8466fb9cc557a.html#a4c7ba0535e6bd550bec8466fb9cc557a", null ],
    [ "GetLog", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_ad109eb924e7af1faaafb925da2a2fa8b.html#ad109eb924e7af1faaafb925da2a2fa8b", null ],
    [ "GetLogAttributeInfo", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_ab6a1e7464d9967a47ab83920a9d86a1a.html#ab6a1e7464d9967a47ab83920a9d86a1a", null ],
    [ "GetLogTimeBoundaries", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a85d4a50209f4a0735628878557f0c983.html#a85d4a50209f4a0735628878557f0c983", null ],
    [ "GetMetaAttributeInfo", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a6d052430a47152b5b3f1824a279141d2.html#a6d052430a47152b5b3f1824a279141d2", null ],
    [ "GetNameClasses", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a2c78ccec29bd7205fa34a0fbdee11dc3.html#a2c78ccec29bd7205fa34a0fbdee11dc3", null ],
    [ "GetResourceClasses", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a5a9ed136126cf224e060adcbac273fb9.html#a5a9ed136126cf224e060adcbac273fb9", null ],
    [ "GetTraceAttributeInfo", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_aeb2ead9aa1da2a0924d6b4c93f6c669d.html#aeb2ead9aa1da2a0924d6b4c93f6c669d", null ],
    [ "GetTraceTimeBoundaries", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a911eba4563955d7f14e92b8c8dc61a17.html#a911eba4563955d7f14e92b8c8dc61a17", null ],
    [ "GetTransitionClasses", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a8588700d232a0296136f7abeb6f65aa1.html#a8588700d232a0296136f7abeb6f65aa1", null ],
    [ "ToString", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a6c8c474246d9c5fcad6906e06fd3fefe.html#a6c8c474246d9c5fcad6906e06fd3fefe", null ],
    [ "NumberOfEvents", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a4d7bb12380728ce24e6ff7b4b018f955.html#a4d7bb12380728ce24e6ff7b4b018f955", null ],
    [ "NumberOfTraces", "interfaceOpenXesNet_1_1info_1_1IXLogInfo_a1f47308beafbd640e44e44ce201fcfe7.html#a1f47308beafbd640e44e44ce201fcfe7", null ]
];