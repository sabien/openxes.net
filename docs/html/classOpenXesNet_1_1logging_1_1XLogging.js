var classOpenXesNet_1_1logging_1_1XLogging =
[
    [ "Importance", "classOpenXesNet_1_1logging_1_1XLogging_a9e269d6cba9fef0293fee5a5b1a0fc91.html#a9e269d6cba9fef0293fee5a5b1a0fc91", [
      [ "TRACE", "classOpenXesNet_1_1logging_1_1XLogging_a9e269d6cba9fef0293fee5a5b1a0fc91.html#a9e269d6cba9fef0293fee5a5b1a0fc91a2d3e4144aa384b18849ab9a8abad74d6", null ],
      [ "DEBUG", "classOpenXesNet_1_1logging_1_1XLogging_a9e269d6cba9fef0293fee5a5b1a0fc91.html#a9e269d6cba9fef0293fee5a5b1a0fc91adc30ec20708ef7b0f641ef78b7880a15", null ],
      [ "INFO", "classOpenXesNet_1_1logging_1_1XLogging_a9e269d6cba9fef0293fee5a5b1a0fc91.html#a9e269d6cba9fef0293fee5a5b1a0fc91a551b723eafd6a31d444fcb2f5920fbd3", null ],
      [ "WARNING", "classOpenXesNet_1_1logging_1_1XLogging_a9e269d6cba9fef0293fee5a5b1a0fc91.html#a9e269d6cba9fef0293fee5a5b1a0fc91a059e9861e0400dfbe05c98a841f3f96b", null ],
      [ "ERROR", "classOpenXesNet_1_1logging_1_1XLogging_a9e269d6cba9fef0293fee5a5b1a0fc91.html#a9e269d6cba9fef0293fee5a5b1a0fc91abb1ca97ec761fc37101737ba0aa2e7c5", null ]
    ] ],
    [ "AddListener", "classOpenXesNet_1_1logging_1_1XLogging_ab18387a8f1c8d0c257b6070cd73b1731.html#ab18387a8f1c8d0c257b6070cd73b1731", null ],
    [ "Debug", "classOpenXesNet_1_1logging_1_1XLogging_a9f6b6525562aac063c3342a0b81d20dd.html#a9f6b6525562aac063c3342a0b81d20dd", null ],
    [ "Error", "classOpenXesNet_1_1logging_1_1XLogging_ad12e2b57e27d6f7b10715d08318272ed.html#ad12e2b57e27d6f7b10715d08318272ed", null ],
    [ "Info", "classOpenXesNet_1_1logging_1_1XLogging_a18a999ce4613e56810680bcbe3061927.html#a18a999ce4613e56810680bcbe3061927", null ],
    [ "Log", "classOpenXesNet_1_1logging_1_1XLogging_ae18a304fabba16039e3d4ff68c0a9172.html#ae18a304fabba16039e3d4ff68c0a9172", null ],
    [ "RemoveListener", "classOpenXesNet_1_1logging_1_1XLogging_ab07245284e28eb44264662bee7a7d41c.html#ab07245284e28eb44264662bee7a7d41c", null ],
    [ "Trace", "classOpenXesNet_1_1logging_1_1XLogging_a7f4114cbb6074e0e7d2640f80c0535e6.html#a7f4114cbb6074e0e7d2640f80c0535e6", null ],
    [ "Warn", "classOpenXesNet_1_1logging_1_1XLogging_a1b02cc843e683a10eb40b925d6418739.html#a1b02cc843e683a10eb40b925d6418739", null ]
];