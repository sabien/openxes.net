var classOpenXesNet_1_1classification_1_1XEventClass =
[
    [ "XEventClass", "classOpenXesNet_1_1classification_1_1XEventClass_ad6894bcab4a5808919ade89bbe0bd78c.html#ad6894bcab4a5808919ade89bbe0bd78c", null ],
    [ "CompareTo", "classOpenXesNet_1_1classification_1_1XEventClass_a3a773e2931c101bfdd8122197328d8d4.html#a3a773e2931c101bfdd8122197328d8d4", null ],
    [ "Equals", "classOpenXesNet_1_1classification_1_1XEventClass_a1eed83aad1cf71d31e0baf373c50c03d.html#a1eed83aad1cf71d31e0baf373c50c03d", null ],
    [ "GetHashCode", "classOpenXesNet_1_1classification_1_1XEventClass_aaf39651190fad0f8a3ee3e68bcd511d7.html#aaf39651190fad0f8a3ee3e68bcd511d7", null ],
    [ "IncrementSize", "classOpenXesNet_1_1classification_1_1XEventClass_a37ca29fcff28b30ab86638fed6b2fa30.html#a37ca29fcff28b30ab86638fed6b2fa30", null ],
    [ "ToString", "classOpenXesNet_1_1classification_1_1XEventClass_a35fdf7d02180fc6ea73655ad53e2c336.html#a35fdf7d02180fc6ea73655ad53e2c336", null ],
    [ "id", "classOpenXesNet_1_1classification_1_1XEventClass_aef0816f3c8a0ed9211eeff1f06df5e25.html#aef0816f3c8a0ed9211eeff1f06df5e25", null ],
    [ "index", "classOpenXesNet_1_1classification_1_1XEventClass_a205215ef1092f69f0a20ce8d29746b8f.html#a205215ef1092f69f0a20ce8d29746b8f", null ],
    [ "size", "classOpenXesNet_1_1classification_1_1XEventClass_ab62317900f0b925f539aacaf3837854c.html#ab62317900f0b925f539aacaf3837854c", null ],
    [ "Id", "classOpenXesNet_1_1classification_1_1XEventClass_ac4264c712afeadde937d17b194937bf9.html#ac4264c712afeadde937d17b194937bf9", null ],
    [ "Index", "classOpenXesNet_1_1classification_1_1XEventClass_a80ece21b6ea3cfcee206ce4b38a12504.html#a80ece21b6ea3cfcee206ce4b38a12504", null ],
    [ "Size", "classOpenXesNet_1_1classification_1_1XEventClass_afcda0ed4ad2e7a6a827b7370eceeb480.html#afcda0ed4ad2e7a6a827b7370eceeb480", null ]
];