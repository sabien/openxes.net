var classOpenXesNet_1_1io_1_1XParser =
[
    [ "CanParse", "classOpenXesNet_1_1io_1_1XParser_a50a02d652dd13f2786bf77a6a688df90.html#a50a02d652dd13f2786bf77a6a688df90", null ],
    [ "Dispose", "classOpenXesNet_1_1io_1_1XParser_a18286fedbc20100dff8a086469ecc4a8.html#a18286fedbc20100dff8a086469ecc4a8", null ],
    [ "EndsWith", "classOpenXesNet_1_1io_1_1XParser_ac87317f744e5c1da0f1f72fb607d3f0a.html#ac87317f744e5c1da0f1f72fb607d3f0a", null ],
    [ "Parse", "classOpenXesNet_1_1io_1_1XParser_a2424320a8d184619c3f3ed16e8a85f1a.html#a2424320a8d184619c3f3ed16e8a85f1a", null ],
    [ "Parse", "classOpenXesNet_1_1io_1_1XParser_a88b098f777705d04ca1a1d18773dd832.html#a88b098f777705d04ca1a1d18773dd832", null ],
    [ "ToString", "classOpenXesNet_1_1io_1_1XParser_a2d4762615c4fddf7aa369ddbc73ed4f2.html#a2d4762615c4fddf7aa369ddbc73ed4f2", null ],
    [ "Author", "classOpenXesNet_1_1io_1_1XParser_add59bd47457b62504cd82010a95b76b7.html#add59bd47457b62504cd82010a95b76b7", null ],
    [ "Description", "classOpenXesNet_1_1io_1_1XParser_af6e6a3be6bb04a1d3fb3eab397c25334.html#af6e6a3be6bb04a1d3fb3eab397c25334", null ],
    [ "Name", "classOpenXesNet_1_1io_1_1XParser_a3725652c172c5a60d4a77916aabf4601.html#a3725652c172c5a60d4a77916aabf4601", null ]
];