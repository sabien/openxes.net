var interfaceOpenXesNet_1_1model_1_1IXLog =
[
    [ "Accept", "interfaceOpenXesNet_1_1model_1_1IXLog_a585386eeac88d45e54ae21a8dd084a60.html#a585386eeac88d45e54ae21a8dd084a60", null ],
    [ "GetInfo", "interfaceOpenXesNet_1_1model_1_1IXLog_afc4b34cfeea1f9365e644c49095b0913.html#afc4b34cfeea1f9365e644c49095b0913", null ],
    [ "SetInfo", "interfaceOpenXesNet_1_1model_1_1IXLog_a16f6316eca9f687eb0448d94e239f73a.html#a16f6316eca9f687eb0448d94e239f73a", null ],
    [ "Classifiers", "interfaceOpenXesNet_1_1model_1_1IXLog_af859c253fda7db1c7b2c3c4d22a77401.html#af859c253fda7db1c7b2c3c4d22a77401", null ],
    [ "GlobalEventAttributes", "interfaceOpenXesNet_1_1model_1_1IXLog_ae49ce1d2094a3cbf0c3e1dc5ae52650a.html#ae49ce1d2094a3cbf0c3e1dc5ae52650a", null ],
    [ "GlobalTraceAttributes", "interfaceOpenXesNet_1_1model_1_1IXLog_ab9f8f238f5389e75fe6caf7fdce45637.html#ab9f8f238f5389e75fe6caf7fdce45637", null ]
];